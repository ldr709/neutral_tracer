#include "gl_utils.h"
#include <fstream>
#include <iostream>

std::string read_shader(const char* path)
{
	std::fstream file(path, std::fstream::in);
	if (!file)
	{
		std::cerr << "Could not read shader \"" << path << "\"" << std::endl;
		exit(EXIT_FAILURE);
	}

	file.seekg(0, std::ios::end);
	size_t size = file.tellg();
	file.seekg(0, std::ios::beg);

	std::string out;
	out.resize(size);

	if (!file.read(&out[0], size))
	{
		std::cerr << "Could not read shader \"" << path << "\"" << std::endl;
		exit(EXIT_FAILURE);
	}

	return out;
}

GLuint compile_shader(const char* path, GLenum type)
{
	GLuint shader = glCreateShader(type);

	std::string shader_source = read_shader(path);
	const char* cstr = shader_source.c_str();
	glShaderSource(shader, 1, &cstr, NULL);

	glCompileShader(shader);

	GLint status = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		// Error in shader, so print error message.
		GLint log_size = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_size);

		char* log = (char*) malloc(log_size);

		glGetShaderInfoLog(shader, log_size, NULL, log);

		std::cerr << "Error compiling shader:" << std::endl;
		std::cerr << log << std::endl;

		free(log);
		exit(EXIT_FAILURE);
	}

	return shader;
}

GLuint create_shader_program(const char* vertex_shader_file,
                             const char* frag_shader_file,
                             const char* geom_shader_file)
{
	GLuint vertex_shader = compile_shader(vertex_shader_file, GL_VERTEX_SHADER);
	GLuint frag_shader;
	if (frag_shader_file)
		frag_shader = compile_shader(frag_shader_file, GL_FRAGMENT_SHADER);
	GLuint geom_shader;
	if (geom_shader_file)
		geom_shader = compile_shader(geom_shader_file, GL_GEOMETRY_SHADER);

	GLuint shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	if (frag_shader_file)
		glAttachShader(shader_program, frag_shader);
	if (geom_shader_file)
		glAttachShader(shader_program, geom_shader);

	glLinkProgram(shader_program);

	GLint status = 0;
	glGetProgramiv(shader_program, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		// Error in shader, so print error message.
		GLint log_size = 0;
		glGetProgramiv(shader_program, GL_INFO_LOG_LENGTH, &log_size);

		char* log = (char*) malloc(log_size);

		glGetProgramInfoLog(shader_program, log_size, NULL, log);

		std::cerr << "Error linking shader:" << std::endl;
		std::cerr << log << std::endl;

		free(log);
		exit(EXIT_FAILURE);
	}

	glDetachShader(shader_program, vertex_shader);
	if (frag_shader_file)
		glDetachShader(shader_program, frag_shader);
	if (geom_shader_file)
		glDetachShader(shader_program, geom_shader);
	glDeleteShader(vertex_shader);
	if (frag_shader_file)
		glDeleteShader(frag_shader);
	if (geom_shader_file)
		glDeleteShader(geom_shader);

	return shader_program;
}
