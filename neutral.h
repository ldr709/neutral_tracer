#ifndef TENSOR_LINES_NEUTRAL_H
#define TENSOR_LINES_NEUTRAL_H

#include <array>
#include <iterator>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/StdVector>
#include "gl_utils.h"
#include "tensor.h"

namespace detail
{
	template<typename NextStage, size_t N>
	struct to_bary_converter;
	template<typename NextStage>
	struct sing_splitter;
	template<typename NextStage>
	struct sing_limiter;
	template<typename NextStage>
	struct tetedge_splitter;
	template<typename NextStage>
	struct edge_splitter;
	template<size_t N>
	struct output_poly;
}

class neutral_tracer_param;

class neutral_tracer
{
	typedef double float_type;

	struct neutral_sphere_vattrs
	{
		GLfloat mid_evec[3];
	};

	struct neutral_surface_vattrs
	{
		GLfloat loc[4];
		GLfloat bary[4];
		GLfloat normal[3];
	};

	typedef Eigen::ColPivHouseholderQR
		<Eigen::Matrix<float_type, 5, 4>> fieldQR_t;

	struct tet_internal
	{
		Eigen::Matrix<float_type, 3, 4> loc;
		Eigen::Matrix<float_type, 3, 3> loc_inv;
		Eigen::Matrix<float_type, 5, 4> field;

		Eigen::Matrix<float_type, 3, 3> eigenvectors;
		Vector3ft eigenvalues;

		fieldQR_t fieldQR;

		// Cokernel of field, represented as two vectors a, b such that the
		// cokernel is (a b^t f b a^t) / 2 - (a^t b / 3) I
		Vector3ft sings[2];
	};

public:
	struct neutral_surface_vert
	{
		Vector3ft_na mid_evec;
		Vector4ft_na bary;
		size_t tet;

		enum
		{
			NORMAL,
			SINGULARITY,
			TETEDGE,
		} kind = NORMAL;
		// Depends on kind:
		// NORMAL:      nothing
		// SINGULARITY: the index of the singularity.
		size_t index;
	};
	typedef std::array<neutral_surface_vert, 3> neutral_surface_tri;
	typedef std::array<neutral_surface_vert, 4> neutral_surface_quad;

private:
	GLuint neutral_sphere_buffer;
	GLuint neutral_sphere_index_buffer;
	GLuint neutral_surface_buffer;
	GLuint lighting_buffer;

	std::vector<tet_internal, Eigen::aligned_allocator<tet_internal>> tets;

	size_t neutral_surface_tri_count;

	static const GLuint neutral_loc_attr = 0;
	static const GLuint neutral_bary_attr = 1;
	static const GLuint neutral_normal_attr = 2;
	GLint neutral_MVP_uniform;
	GLint neutral_lighting_uniform;

	GLuint neutral_shader_program;
	GLuint neutral_vao;

	std::vector<Vector3ft_na> neutral_sphere_verts;
	std::vector<std::array<size_t, 3>> neutral_sphere_tris;

	unsigned int elliptic_x_cells;
	unsigned int elliptic_y_cells;

	void init_gl(const char* shader_prefix);
	void create_neutral_sphere(unsigned int subdivisions);

	static Vector4ft
	field_mid_to_loc(const Eigen::Matrix<float_type, 3, 4>& field_mid_mat);
	static Vector4ft mid_evec_to_bary(const tet_internal& tet, Vector3ft mid);
	static Vector3ft mid_evec_to_normal(const tet_internal& tet, Vector3ft mid);
	static Vector3ft mid_evec_to_normal(
		const Eigen::Matrix<float_type, 3, 4>& field_mid_mat,
		const Matrix3ft& loc_inv, Vector3ft mid);
	static neutral_surface_vert intersect_face_sphere_edge(
		size_t tet_i, const tet_internal& tet, unsigned int face,
		const Eigen::Matrix<float_type, 5, 3>& face_field,
		const neutral_surface_vert* end_points[2]);
	static neutral_surface_vert intersect_face_sing(
		size_t tet_i, unsigned int face, unsigned int index, Vector3ft mid_evec,
		Vector4ft adj_bary[2]);
	static std::pair<neutral_surface_vert, bool>
	intersect_face_sphere_edge_endpoint_root(
		size_t tet_i, const tet_internal& tet,
		unsigned int face, const Eigen::Matrix<float_type, 5, 3>& face_field,
		const neutral_surface_vert* end_points[2]);
	static neutral_surface_vert intersect_face_sphere_edge_no_sing(
		size_t tet_i, const tet_internal& tet, unsigned int face,
		const Eigen::Matrix<float_type, 5, 3>& face_field, Vector3ft mid[2],
		const Vector4ft& bary_nearby);
	static Vector4ft sing_loc(
		const Eigen::Matrix<float_type, 3, 4>& field_mid_mat,
		const Eigen::Matrix<float_type, 3, 4>& adjacent);
	static Eigen::Matrix<float_type, 3, 4>
	get_field_mid_mat(const tet_internal& tet, Vector3ft mid);

public:
	std::array<std::vector<Vector3ft_na>, 2>
	sample_face_intersection(size_t tet_i, unsigned int face);

private:
	template<typename NextStage>
	void icos_triangles(NextStage& ns, unsigned int tet_i) const;
	template<typename NextStage>
	void elliptic_quads(NextStage& ns, unsigned int tet_i) const;
	template<typename NextStage>
	void jacobi_elliptic_quads(NextStage& ns, unsigned int tet_i) const;

	static Vector3ft elliptic_param(float_type f_sq_m1, const Vector2ft& p);
	static Vector2ft elliptic_inv_param(
		float_type f, float_type f_sq, const Vector3ft& m);

	template<typename NextStage, size_t N>
	friend struct detail::to_bary_converter;
	template<typename NextStage>
	friend struct detail::sing_splitter;
	template<typename NextStage>
	friend struct detail::sing_limiter;
	template<typename NextStage>
	friend struct detail::tetedge_splitter;
	template<typename NextStage>
	friend struct detail::edge_splitter;
	template<size_t N>
	friend struct detail::output_poly;

	friend class neutral_tracer_param;

	std::vector<neutral_surface_tri> surface;

public:
	enum neutral_mesh_kind
	{
		ICOSPHERE,
		ELLIPTIC,
		JACOBI_ELLIPTIC
	};

	typedef detail::detail_tetrahedron tetrahedron;

	struct light_buf
	{
		float uLightPos[4];
		float uLightSpecularColor[4];
		float uEyePos[4];
		float uKa;
		float uKd;
		float uKs;
		float uShininess;
	};

	// It must be an input iterator of tetrahedron's
	template<typename It>
	neutral_tracer(const char* shader_prefix, unsigned int subdivisions,
	               unsigned int elliptic_x_cells, unsigned int elliptic_y_cells,
	               It tets_start, It tets_end);
	~neutral_tracer();

	neutral_tracer(const neutral_tracer&) = delete;
	neutral_tracer& operator=(neutral_tracer&) = delete;

	light_buf get_lighting();
	void set_lighting(light_buf lighting);

	void preprocess(neutral_mesh_kind mesh_kind);
	void render(const Matrix4gf& mvp);

	// For getting neutral surface from C++
	struct neutral_surface_output_vert
	{
		Vector3ft_na loc;
		Vector3ft_na mid_evec;
		Vector3ft_na normal;
	};
	typedef std::array<neutral_surface_output_vert, 3>
		neutral_surface_output_tri;
	typedef std::array<neutral_surface_output_vert, 4>
		neutral_surface_output_quad;

	const std::vector<neutral_surface_output_tri>& getNeutralSurface() const
	{
		return m_surfTriangleLocations;
	}

	std::vector<neutral_surface_output_quad>
	get_neutral_surface_quads(neutral_mesh_kind mesh_kind) const;

private:
	std::vector<neutral_surface_output_tri> m_surfTriangleLocations;
};

template<typename It>
neutral_tracer::neutral_tracer(const char* shader_prefix,
                               unsigned int subdivisions,
                               unsigned int elliptic_x_cells,
                               unsigned int elliptic_y_cells,
                               It tets_start, It tets_end) :
    elliptic_x_cells(elliptic_x_cells),
    elliptic_y_cells(elliptic_y_cells)
{
	size_t num_tets = std::distance(tets_start, tets_end);

	create_neutral_sphere(subdivisions);
	init_gl(shader_prefix);


	tets.resize(num_tets);
	size_t i = 0;
	for (It it = tets_start; it != tets_end; ++it, ++i)
	{
		const tetrahedron& p = *it;
		tet_internal& t = tets[i];

		t.loc = p.loc;
		t.field = p.field;

		t.loc_inv =
			(p.loc.rightCols<3>() - p.loc.col(0).replicate<1, 3>()).inverse();

		t.fieldQR.compute(p.field);
		tensor3 cokernel = t.fieldQR.householderQ() * tensor3::Unit(4);
		Matrix3ft cokernel_quad = tensor3_to_quadratic_eq(cokernel);

		// Make it neutral or linear for convenience.
		if (cokernel_quad.determinant() < 0.0)
			cokernel_quad *= -1;

		// Find the singularities' medium eigenvectors.
		Eigen::SelfAdjointEigenSolver<Matrix3ft> eigen(cokernel_quad);
		float_type cokernel_min = eigen.eigenvalues()[0];
		float_type cokernel_mid = eigen.eigenvalues()[1];
		float_type cokernel_max = eigen.eigenvalues()[2];

		float_type sqrt_min = sqrt(cokernel_mid - cokernel_min);
		float_type sqrt_max = sqrt(cokernel_max - cokernel_mid);
		Vector2ft normal0(sqrt_min, sqrt_max);
		normal0.normalize();
		t.sings[0] =
			(eigen.eigenvectors() * Vector3ft(normal0[0], 0.0, normal0[1]));
		t.sings[1] =
			(eigen.eigenvectors() * Vector3ft(normal0[0], 0.0, -normal0[1]));

		t.eigenvectors = eigen.eigenvectors();
		t.eigenvalues = eigen.eigenvalues();

		// Use a right-handed system of eigenvectors.
		if (t.eigenvectors.determinant() < 0.0)
			t.eigenvectors.col(1) = -t.eigenvectors.col(1);
	}
}
#endif
