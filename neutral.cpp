#include <complex>
#include <limits>
#include <map>
#include <math.h>
#include <omp.h>
#include <string>
#include <vector>
#include "neutral.h"
#include "quadratic.h"
#include "quaternion_group.h"

template<typename MatrixType, typename ResultType>
static inline void adjugate_3x3(const MatrixType& matrix, ResultType& result);

void neutral_tracer::init_gl(const char* shader_prefix)
{
	glGenBuffers(1, &neutral_sphere_buffer);
	glGenBuffers(1, &neutral_sphere_index_buffer);
	glGenBuffers(1, &lighting_buffer);
	glGenBuffers(1, &neutral_surface_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, neutral_sphere_buffer);
	{
		// Write the data to the buffer.
		glBufferData(GL_ARRAY_BUFFER,
		             neutral_sphere_verts.size() * sizeof(neutral_sphere_vattrs),
		             NULL, GL_STATIC_DRAW);
		neutral_sphere_vattrs* attrs = (neutral_sphere_vattrs*)
			glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		for (const auto& mid_evec : neutral_sphere_verts)
		{
			Eigen::Map<Eigen::Matrix<GLfloat, 3, 1> >(attrs->mid_evec) =
				mid_evec.cast<GLfloat>();

			attrs++;
		}

		glUnmapBuffer(GL_ARRAY_BUFFER);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, neutral_sphere_index_buffer);
	{
		// Write the data to the buffer.
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		             neutral_sphere_tris.size() * 3 * sizeof(GLushort),
		             NULL, GL_STATIC_DRAW);
		GLushort* attrs = (GLushort*)
			glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

		for (const auto& tri : neutral_sphere_tris)
		{
			*(attrs++) = tri[0];
			*(attrs++) = tri[1];
			*(attrs++) = tri[2];
		}

		glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_UNIFORM_BUFFER, lighting_buffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(light_buf), NULL, GL_STREAM_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	std::string vert_file = std::string(shader_prefix) + "neutral.vert";
	std::string frag_file = std::string(shader_prefix) + "neutral.frag";
	neutral_shader_program = create_shader_program(
		vert_file.c_str(),
		frag_file.c_str());
	neutral_MVP_uniform = glGetUniformLocation(neutral_shader_program, "MVP");
	neutral_lighting_uniform =
		glGetUniformBlockIndex(neutral_shader_program, "lightBuf");
	glUniformBlockBinding(neutral_shader_program, neutral_lighting_uniform, 0);

	glGenVertexArrays(1, &neutral_vao);

	glBindVertexArray(neutral_vao);
	glBindBuffer(GL_ARRAY_BUFFER, neutral_surface_buffer);
	glEnableVertexAttribArray(neutral_loc_attr);
	glVertexAttribPointer(neutral_loc_attr, 4, GL_FLOAT, GL_FALSE,
	                      sizeof(neutral_surface_vattrs),
	                      (void*) offsetof(neutral_surface_vattrs, loc));
	glEnableVertexAttribArray(neutral_bary_attr);
	glVertexAttribPointer(neutral_bary_attr, 4, GL_FLOAT, GL_FALSE,
	                      sizeof(neutral_surface_vattrs),
	                      (void*) offsetof(neutral_surface_vattrs, bary));
	glEnableVertexAttribArray(neutral_normal_attr);
	glVertexAttribPointer(neutral_normal_attr, 3, GL_FLOAT, GL_FALSE,
	                      sizeof(neutral_surface_vattrs),
	                      (void*) offsetof(neutral_surface_vattrs, normal));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

neutral_tracer::~neutral_tracer()
{
	glDeleteVertexArrays(1, &neutral_vao);
	glDeleteBuffers(1, &neutral_sphere_buffer);
	glDeleteBuffers(1, &neutral_sphere_index_buffer);
	glDeleteBuffers(1, &lighting_buffer);
	glDeleteBuffers(1, &neutral_surface_buffer);
	glDeleteProgram(neutral_shader_program);
}

neutral_tracer::light_buf neutral_tracer::get_lighting()
{
	light_buf lighting_out;
	glBindBuffer(GL_UNIFORM_BUFFER, lighting_buffer);
	light_buf* lighting = (light_buf*)
		glMapBuffer(GL_UNIFORM_BUFFER, GL_READ_ONLY);
	lighting_out = *lighting;
	glUnmapBuffer(GL_UNIFORM_BUFFER);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	return lighting_out;
}

void neutral_tracer::set_lighting(light_buf lighting)
{
	glBindBuffer(GL_UNIFORM_BUFFER, lighting_buffer);
	light_buf* lighting_map = (light_buf*)
		glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
	*lighting_map = lighting;
	glUnmapBuffer(GL_UNIFORM_BUFFER);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

namespace detail
{

// Quad in CCW order.
template<typename NextStage>
struct quad_splitter
{
	NextStage& ns;

	quad_splitter(NextStage& ns_) : ns(ns_) {}

	void operator()(neutral_tracer::neutral_surface_quad quad)
	{
		// Check spherical delaunay / convex hull.
		Vector3ft v0 = quad[0].mid_evec;
		Vector3ft normal =
			(quad[1].mid_evec - v0).cross(quad[3].mid_evec - v0);
		if (normal.dot(quad[2].mid_evec - v0) > 0.0)
		{
			std::swap(quad[0], quad[1]);
			std::swap(quad[1], quad[2]);
			std::swap(quad[2], quad[3]);
		}

		ns({quad[0], quad[1], quad[3]});
		ns({quad[1], quad[2], quad[3]});
	}

	void done()
	{
		ns.done();
	}
};

template<typename NextStage, size_t N>
struct to_bary_converter
{
	NextStage& ns;
	const neutral_tracer::tet_internal& tet;

	to_bary_converter(NextStage& ns_,
	                  const neutral_tracer::tet_internal& tet_) :
		ns(ns_),
		tet(tet_) {}

private:
	typedef neutral_tracer::neutral_surface_vert ns_vert;

	void convert_vert(ns_vert& v)
	{
		if (v.kind != ns_vert::SINGULARITY)
			v.bary = neutral_tracer::mid_evec_to_bary(tet, v.mid_evec);
	}

public:
	void operator()(std::array<ns_vert, N> poly)
	{
		for (unsigned int j = 0; j < N; j++)
			convert_vert(poly[j]);
		ns(poly);
	}

	//// Allow some type conversions.
	//void operator()(neutral_tracer::neutral_surface_tri tri)
	//{
	//	(*this)(tri);
	//}
	//void operator()(neutral_tracer::neutral_surface_quad quad)
	//{
	//	(*this)(quad);
	//}

	void done()
	{
		ns.done();
	}
};

template<typename NextStage>
using to_bary_tri = to_bary_converter<NextStage, 3>;

template<typename NextStage>
using to_bary_quad = to_bary_converter<NextStage, 4>;

template<typename NextStage>
struct sing_splitter
{
	NextStage& ns;
	unsigned int sing;
	const neutral_tracer::tet_internal& tet;

	std::vector<neutral_tracer::neutral_surface_vert> to_triangulate[2];

	sing_splitter(NextStage& ns_, unsigned int sing_,
	              const neutral_tracer::tet_internal& tet_) :
		ns(ns_),
		sing(sing_),
		tet(tet_) {}

	void operator()(neutral_tracer::neutral_surface_tri tri)
	{
		bool is_sing = false;
		bool is_reversed = false;
		Vector3ft sing_mid = tet.sings[sing];
		Vector3ft v0 = tri[0].mid_evec;
		Vector3ft normal = (tri[1].mid_evec - v0).cross(tri[2].mid_evec - v0);

		// TODO: Verify stability.
		if (normal.dot(sing_mid - v0) >= 0.0)
			is_sing = true;
		else if (normal.dot(-sing_mid - v0) >= 0.0)
		{
			is_sing = true;
			is_reversed = true;
		}

		if (is_sing)
		{
			auto& vec = to_triangulate[is_reversed];
			vec.insert(vec.end(), tri.begin(), tri.end());
		}
		else
			ns(tri);
	}

	void done()
	{
		typedef neutral_tracer::neutral_surface_vert ns_vert;

		for (unsigned int is_reversed = 0; is_reversed < 2; is_reversed++)
		{
			auto& vec = to_triangulate[is_reversed];
			//assert(!vec.empty());
			if (vec.empty())
			{
				continue;
			}


			Vector3ft sing_mid = tet.sings[sing];
			if (is_reversed)
				sing_mid = -sing_mid;
			Vector3ft side_vec;
			{
				Vector3ft side_vec0 = sing_mid.cross(Vector3ft::Unit(0));
				Vector3ft side_vec1 = sing_mid.cross(Vector3ft::Unit(1));
				float_type norm_sq0 = side_vec0.squaredNorm();
				float_type norm_sq1 = side_vec1.squaredNorm();
				side_vec = norm_sq0 >= norm_sq1 ?
					side_vec0 / sqrt(norm_sq0) : side_vec1 / sqrt(norm_sq1);
			}
			Vector3ft up_vec = sing_mid.cross(side_vec);

			typedef std::pair<float_type, size_t> angle_type;
			std::vector<angle_type> angles;
			for (size_t i = 0; i < vec.size(); i++)
			{
				Vector3ft v = vec[i].mid_evec;
				float_type c = v.dot(side_vec);
				float_type s = v.dot(up_vec);
				angles.push_back({atan2(s, c), i});
			}
			std::sort(angles.begin(), angles.end());
			angles.erase(std::unique(angles.begin(), angles.end(),
				[&] (const angle_type& a, const angle_type& b)
				{
					return vec[a.second].mid_evec == vec[b.second].mid_evec;
				}), angles.end());

			ns_vert sing_v;
			sing_v.mid_evec = sing_mid;
			sing_v.bary = Vector4ft::Zero();
			sing_v.tet = vec.front().tet;
			sing_v.kind = ns_vert::SINGULARITY;
			sing_v.index = sing;

			const auto* prev_vert = &vec[angles.back().second];
			for (const auto& vert_angle : angles)
			{
				const ns_vert& vert = vec[vert_angle.second];

				// Recursively re-triangulate.
				ns({*prev_vert, vert, sing_v});

				prev_vert = &vert;
			}

			vec.clear();
		}

		ns.done();
	}
};

template<typename NextStage>
struct sing_limiter
{
	NextStage& ns;
	unsigned int sing;
	const neutral_tracer::tet_internal& tet;

	sing_limiter(NextStage& ns_, unsigned int sing_,
	             const neutral_tracer::tet_internal& tet_) :
		ns(ns_),
		sing(sing_),
		tet(tet_) {}

	void operator()(neutral_tracer::neutral_surface_tri tri)
	{
		typedef neutral_tracer::neutral_surface_vert ns_vert;

		int sing_vert = -1;
		for (unsigned int i = 0; i < 3; i++)
		{
			if (tri[i].kind == ns_vert::SINGULARITY && tri[i].index == sing)
			{
				sing_vert = i;
				break;
			}
		}

		if (sing_vert >= 0)
		{
			Vector3ft sing_mid = tri[sing_vert].mid_evec;
			Eigen::Matrix<float_type, 3, 4> field_mid_mat =
				neutral_tracer::get_field_mid_mat(tet, sing_mid);

			// Make the singularity be two vertices, one for each way to
			// approach it.
			ns_vert sing_vs[2];
			for (unsigned int i = 0; i < 2; i++)
			{
				unsigned int v = (sing_vert + i + 1) % 3;
				Eigen::Matrix<float_type, 3, 4> adjacent_mid_mat =
					neutral_tracer::get_field_mid_mat(tet, tri[v].mid_evec);

				sing_vs[i].mid_evec = sing_mid;
				sing_vs[i].bary =
					neutral_tracer::sing_loc(field_mid_mat, adjacent_mid_mat);
				sing_vs[i].tet = tri[0].tet;
				sing_vs[i].kind = ns_vert::SINGULARITY;
				sing_vs[i].index = sing;
			}

			// Split the triangle into a quad.
			ns({tri[(sing_vert + 1) % 3], tri[(sing_vert + 2) % 3],
			    sing_vs[0]});
			ns({sing_vs[0], tri[(sing_vert + 2) % 3], sing_vs[1]});
		}
		else
			ns(tri);
	}

	void done()
	{
		ns.done();
	}
};

// Does spherical delaunay / convex hull with bowyer-watson.
template<typename NextStage>
struct tetedge_splitter
{
	NextStage& ns;
	const neutral_tracer::tet_internal& tet;
	unsigned int edge; // & 0xff gives first vertex, >> 8 gives second.
	Vector3ft intersections[3];
	Vector4ft intersection_barys[3];
	unsigned int num_intersections;
	unsigned int finished_intersections;

	std::vector<neutral_tracer::neutral_surface_vert> to_triangulate[3];

	tetedge_splitter(NextStage& ns_, unsigned int edge_,
	                 const neutral_tracer::tet_internal& tet_) :
		ns(ns_),
		edge(edge_),
		tet(tet_)
	{
		unsigned int v_indexes[2] = {edge & 0xff, edge >> 8};
		Matrix3ft field[2];
		for (unsigned int v = 0; v < 2; v++)
			field[v] = tensor3_to_matrix(tet.field.col(v_indexes[v]));

		Eigen::GeneralizedEigenSolver<Matrix3ft> solver(field[0], field[1]);

		// TODO: Maybe use determinant to find out how many solutions to use.
		num_intersections = 0;
		finished_intersections = 0;
		for (unsigned int i = 0; i < 3; i++)
		{
			std::complex<float_type> alpha = solver.alphas()[i];
			float_type beta = solver.betas()[i];

			// Don't bother with complex roots.
			if (alpha.imag() != 0.0)
				continue;

			// det(beta*A + -alpha*B) = 0
			Vector2ft solution(beta, -alpha.real());
			if (solution.x() < 0.0)
				solution = -solution;
			if (solution.y() < 0.0)
				continue;

			Vector3ft mid_evec = solver.eigenvectors().col(i).real();

			Vector4ft bary = Vector4ft::Zero();
			Vector4ft bary_tmp =
				neutral_tracer::mid_evec_to_bary(tet, mid_evec);
			unsigned int v_indexes[2] = {edge & 0xff, edge >> 8};
			bary[v_indexes[0]] = bary_tmp[v_indexes[0]];
			bary[v_indexes[1]] = bary_tmp[v_indexes[1]];

			if (bary.sum() < 0.0)
			{
				bary = -bary;
				mid_evec = -mid_evec;
			}

			intersections[num_intersections] = mid_evec;
			intersection_barys[num_intersections] = bary;
			num_intersections++;
		}
	}

	void operator()(neutral_tracer::neutral_surface_tri tri)
	{
		int intersection = -1;
		Vector3ft v0 = tri[0].mid_evec;
		Vector3ft normal = (tri[1].mid_evec - v0).cross(tri[2].mid_evec - v0);

		// TODO: Verify stability.
		for (unsigned int i = finished_intersections;
		     i < num_intersections; i++)
		{
			Vector3ft int_mid = intersections[i];
			if (normal.dot(int_mid - v0) >= 0.0)
			{
				intersection = i;
				break;
			}
		}

		if (intersection >= 0)
		{
			// Save the vertices for later re-triangulation.
			auto& vec = to_triangulate[intersection];
			vec.insert(vec.end(), tri.begin(), tri.end());
		}
		else
			ns(tri);
	}

	void done()
	{
		typedef neutral_tracer::neutral_surface_vert ns_vert;

		for (unsigned int i = 0; i < num_intersections; i++)
		{
			auto& vec = to_triangulate[i];
			//assert(!vec.empty());
			if (vec.empty())
			{
				continue;
			}

			Vector3ft int_mid = intersections[i];
			Vector3ft side_vec;
			{
				Vector3ft side_vec0 = int_mid.cross(Vector3ft::Unit(0));
				Vector3ft side_vec1 = int_mid.cross(Vector3ft::Unit(1));
				float_type norm_sq0 = side_vec0.squaredNorm();
				float_type norm_sq1 = side_vec1.squaredNorm();
				side_vec = norm_sq0 >= norm_sq1 ?
					side_vec0 / sqrt(norm_sq0) : side_vec1 / sqrt(norm_sq1);
			}
			Vector3ft up_vec = int_mid.cross(side_vec);

			typedef std::pair<float_type, size_t> angle_type;
			std::vector<angle_type> angles;
			for (size_t i = 0; i < vec.size(); i++)
			{
				Vector3ft v = vec[i].mid_evec;
				float_type c = v.dot(side_vec);
				float_type s = v.dot(up_vec);
				angles.push_back({atan2(s, c), i});
			}
			std::sort(angles.begin(), angles.end());
			angles.erase(std::unique(angles.begin(), angles.end(),
				[&] (const angle_type& a, const angle_type& b)
				{
					return vec[a.second].mid_evec == vec[b.second].mid_evec;
				}), angles.end());

			ns_vert int_v;
			int_v.mid_evec = int_mid;
			int_v.bary = intersection_barys[i];
			int_v.tet = vec.front().tet;
			int_v.kind = ns_vert::TETEDGE;
			int_v.index = edge;

			const auto* prev_vert = &vec[angles.back().second];
			finished_intersections = i + 1;
			for (const auto& vert_angle : angles)
			{
				const ns_vert& vert = vec[vert_angle.second];

				// Recursively re-triangulate.
				(*this)({*prev_vert, vert, int_v});

				prev_vert = &vert;
			}

			vec.clear();
		}

		ns.done();
	}
};

template<typename NextStage>
struct triangle_culler
{
	NextStage& ns;

	triangle_culler(NextStage& ns_) : ns(ns_) {}

	void operator()(neutral_tracer::neutral_surface_tri tri)
	{
		Eigen::Matrix<float_type, 4, 3> bary;
		for (unsigned int i = 0; i < 3; i++)
			bary.col(i) = tri[i].bary;

		// Cull the triangles.
		if ((bary.array() < 0.0).rowwise().all().any())
			return;

		ns(tri);
	}

	void done()
	{
		ns.done();
	}
};

template<typename NextStage>
struct edge_splitter
{
	NextStage& ns;
	unsigned int face;
	const neutral_tracer::tet_internal& tet;
	Eigen::Matrix<float_type, 5, 3> face_field;

	edge_splitter(NextStage& ns_, unsigned int face_,
	              const neutral_tracer::tet_internal& tet_) :
		ns(ns_),
		face(face_),
		tet(tet_)
	{
		for (unsigned int i = 0; i < 3; i++)
			face_field.col(i) = tet.field.col(i + (face <= i));
	}

	void operator()(neutral_tracer::neutral_surface_tri tri)
	{
		typedef neutral_tracer::neutral_surface_vert ns_vert;

		size_t tet_i = tri[0].tet;

		Eigen::Matrix<float_type, 4, 3> bary;
		for (unsigned int i = 0; i < 3; i++)
			bary.col(i) = tri[i].bary;

		unsigned int verts_inside = (bary.row(face).array() >= 0.0).count();
		if (verts_inside == 3)
			ns(tri);
		else if (verts_inside == 1)
		{
			// Clip the triangle to get another triangle.
			unsigned int inside_v;
			for (inside_v = 0; inside_v < 3; inside_v++)
				if (bary(face, inside_v) >= 0.0)
					break;
			assert(inside_v < 3); // There should be something inside.

			if (tri[inside_v].kind == ns_vert::TETEDGE &&
			    (tri[inside_v].index & 0xff) != face &&
			    (tri[inside_v].index >> 8) != face)
			{
				for (unsigned int j = 1; j <= 2; j++)
				{
					unsigned int outside_v = (inside_v + j) % 3;
					const ns_vert* endpoints[2] =
						{&tri[outside_v], &tri[inside_v]};
					auto intersection = neutral_tracer::
						intersect_face_sphere_edge_endpoint_root(
							tet_i, tet, face, face_field, endpoints);
					if (!intersection.second)
						return;
					tri[outside_v] = intersection.first;
					if ((tri[outside_v].bary.head(face).array() < 0.0).any())
						return;
				}
			}
			else
			{
				for (unsigned int j = 1; j <= 2; j++)
				{
					unsigned int outside_v = (inside_v + j) % 3;
					const ns_vert* endpoints[2] =
						{&tri[inside_v], &tri[outside_v]};
					tri[outside_v] = neutral_tracer::intersect_face_sphere_edge(
						tet_i, tet, face, face_field, endpoints);
					if ((tri[outside_v].bary.head(face).array() < 0.0).any())
						// Somehow got pushed outside. Abort!
						return;
				}
			}

			ns(tri);
		}
		else if (verts_inside == 2)
		{
			unsigned int outside_v;
			for (outside_v = 0; outside_v < 3; outside_v++)
				if (bary(face, outside_v) < 0.0)
					break;
			assert(outside_v < 3); // There should be something outside.

			unsigned int inside_vs[2];
			neutral_tracer::neutral_surface_vert split_verts[2];
			bool skip_split[2] = {false, false};
			for (unsigned int i = 0; i < 2; i++)
			{
				unsigned int inside_v = (outside_v + 1 + i) % 3;
				inside_vs[i] = inside_v;

				// If the inside vertex is already split against this face,
				// don't do this split.
				if (tri[inside_v].kind == ns_vert::TETEDGE &&
				    (tri[inside_v].index & 0xff) != face &&
				    (tri[inside_v].index >> 8) != face)
				{
					const ns_vert* endpoints[2] =
						{&tri[outside_v], &tri[inside_v]};
					auto intersection = neutral_tracer::
						intersect_face_sphere_edge_endpoint_root(
							tet_i, tet, face, face_field, endpoints);

					if (!intersection.second)
					{
						skip_split[i] = true;
						continue;
					}

					split_verts[i] = intersection.first;
					if ((split_verts[i].bary.head(face).array() < 0.0).any())
						skip_split[i] = true;
					continue;
				}

				const ns_vert* endpoints[2] = {&tri[inside_v], &tri[outside_v]};
				split_verts[i] = neutral_tracer::intersect_face_sphere_edge(
					tet_i, tet, face, face_field, endpoints);

				if ((split_verts[i].bary.head(face).array() < 0.0).any())
					skip_split[i] = true;
			}

			if (!skip_split[0])
				ns({tri[inside_vs[0]],
				    skip_split[1] ? tri[inside_vs[1]] : split_verts[1],
				    split_verts[0]});
			if (!skip_split[1])
				ns({tri[inside_vs[1]], split_verts[1], tri[inside_vs[0]]});
		}
	}

	void done()
	{
		ns.done();
	}
};

template<size_t N>
struct output_poly
{
	typedef std::array<neutral_tracer::neutral_surface_vert, N> ns_poly;
	std::vector<ns_poly>& output;

	void operator()(ns_poly poly)
	{
//		for (unsigned int i = 0; i < N; i++)
//			if (poly[i].bary.sum() < 0.0)
//				return;
		output.push_back(poly);
	}

	void done() {}
};

template<template<class> class Operation, typename NextStage,
         typename ...Params>
Operation<NextStage> make_op(NextStage& ns, Params&&... params)
{
	return Operation<NextStage>(ns, std::forward<Params>(params)...);
}

}

void neutral_tracer::preprocess(neutral_mesh_kind mesh_kind)
{
	std::unique_ptr<std::vector<neutral_surface_tri>[]> surface_parts;
	#pragma omp parallel
	{
		#pragma omp single
		surface_parts.reset(
			new std::vector<neutral_surface_tri>[omp_get_num_threads()]());

		#pragma omp for
		for (int i = 0; i < tets.size(); i++)
		{
			const tet_internal& tet = tets[i];

			using namespace detail;

			detail::output_poly<3> outputter{surface_parts[omp_get_thread_num()]};
			auto splitter_e3 = make_op<edge_splitter>(outputter, 3, tet);
			auto splitter_e2 = make_op<edge_splitter>(splitter_e3, 2, tet);
			auto splitter_e1 = make_op<edge_splitter>(splitter_e2, 1, tet);
			auto splitter_e0 = make_op<edge_splitter>(splitter_e1, 0, tet);
			auto culler      = make_op<triangle_culler>(splitter_e0);
			auto limiter_s1  = make_op<sing_limiter>(culler, 1, tet);
			auto limiter_s0  = make_op<sing_limiter>(limiter_s1, 0, tet);
			auto splitter_t5 = make_op<tetedge_splitter>(limiter_s0, 0x203, tet);
			auto splitter_t4 = make_op<tetedge_splitter>(splitter_t5, 0x103, tet);
			auto splitter_t3 = make_op<tetedge_splitter>(splitter_t4, 0x102, tet);
			auto splitter_t2 = make_op<tetedge_splitter>(splitter_t3, 0x003, tet);
			auto splitter_t1 = make_op<tetedge_splitter>(splitter_t2, 0x002, tet);
			auto splitter_t0 = make_op<tetedge_splitter>(splitter_t1, 0x001, tet);
			auto bary_conv   = make_op<to_bary_tri>(splitter_t0, tet);

			auto& stage0 = bary_conv;

			auto stage0_quads = make_op<quad_splitter>(stage0);

			switch (mesh_kind)
			{
			case ICOSPHERE:
				icos_triangles(stage0, i);
				break;

			case ELLIPTIC:
				elliptic_quads(stage0_quads, i);
				break;

			case JACOBI_ELLIPTIC:
				jacobi_elliptic_quads(stage0_quads, i);
				break;
			}
		}

		#pragma omp single
		for (int i = 0; i < omp_get_num_threads(); i++)
			surface.insert(surface.end(),
			               surface_parts[i].begin(), surface_parts[i].end());
	}
	surface_parts.reset();

	m_surfTriangleLocations.clear();
	neutral_surface_tri_count = surface.size();
	glBindBuffer(GL_ARRAY_BUFFER, neutral_surface_buffer);
	{
		// Write the data to the buffer.
		glBufferData(
			GL_ARRAY_BUFFER,
			3 * neutral_surface_tri_count * sizeof(neutral_surface_vattrs),
			NULL, GL_STATIC_DRAW);
		neutral_surface_vattrs* attrs = (neutral_surface_vattrs*)
			glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		for (const auto& tri : surface)
		{
			neutral_surface_output_tri currentTriangleToAdd;
			int counter = 0;
			bool addToHybridMesh = true;

			for (const auto& p : tri)
			{
				Vector3ft mid = p.mid_evec;
				Vector4ft bary = p.bary;
				float_type scale = 1.0 / std::abs(bary.sum());
				bary *= scale;
				//mid *= scale;
				const tet_internal& tet = tets[p.tet];

				Matrix4ft tet_loc_homog = tet.loc.colwise().homogeneous();
				Vector4ft loc = tet_loc_homog * bary;
				Vector3ft normal = mid_evec_to_normal(tet, mid);//.normalized();
				normal *= scale;

				Eigen::Map<Eigen::Matrix<GLfloat, 4, 1> >(attrs->loc) <<
					loc.cast<GLfloat>();
				//	loc.cast<GLfloat>(), 1.0;
				//	mid.cast<GLfloat>(), 1.0;
				Eigen::Map<Eigen::Matrix<GLfloat, 4, 1> >(attrs->bary) <<
					mid.cast<GLfloat>(), 1.0;
					//bary.cast<GLfloat>();
				Eigen::Map<Eigen::Matrix<GLfloat, 3, 1> >(attrs->normal) =
					normal.cast<GLfloat>();
				//	mid.cast<GLfloat>();
				attrs++;

				// Add triangles for hybrid display
				Vector3ft locP = (tet_loc_homog * bary).hnormalized();
				currentTriangleToAdd[counter].loc = locP;
				currentTriangleToAdd[counter].mid_evec = mid;
				currentTriangleToAdd[counter++].normal = normal;

				if (bary[0] < 0 || bary[1] < 0 || bary[2] < 0 || bary[3] < 0)
				{
					addToHybridMesh = false;
				}
			}

			if(addToHybridMesh)
				m_surfTriangleLocations.push_back(currentTriangleToAdd);
		}

		glUnmapBuffer(GL_ARRAY_BUFFER);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

std::vector<neutral_tracer::neutral_surface_output_quad>
neutral_tracer::get_neutral_surface_quads(neutral_mesh_kind mesh_kind) const
{
	std::vector<neutral_surface_quad> surface_quads;
	for (int i = 0; i < tets.size(); i++)
	{
		const tet_internal& tet = tets[i];

		using namespace detail;

		detail::output_poly<4> outputter{surface_quads};
		auto bary_conv = make_op<to_bary_quad>(outputter, tet);

		auto& stage0 = bary_conv;

		switch (mesh_kind)
		{
		case ELLIPTIC:
			elliptic_quads(stage0, i);
			break;

		case JACOBI_ELLIPTIC:
			jacobi_elliptic_quads(stage0, i);
			break;

		default:
			assert(0);
			break;
		}
	}

	std::vector<neutral_surface_output_quad> output_quads;
	for (const auto& quad : surface_quads)
	{
		neutral_surface_output_quad output_quad;
		int i = 0;
		bool infinite = false;

		for (const auto& p : quad)
		{
			Vector3ft mid = p.mid_evec;
			Vector4ft bary = p.bary;
			float_type scale = 1.0 / std::abs(bary.sum());
			bary *= scale;

			const tet_internal& tet = tets[p.tet];
			Vector3ft normal = mid_evec_to_normal(tet, mid);
			normal *= scale;

			Matrix4ft tet_loc_homog = tet.loc.colwise().homogeneous();
			Vector4ft loc = tet_loc_homog * bary;
			Vector3ft loc_p = loc.hnormalized();
			output_quad[i].loc = loc_p;
			output_quad[i].mid_evec = mid;
			output_quad[i++].normal = normal;

			if (loc.w() <= 0.0)
				infinite = true;
		}

		if (!infinite)
			output_quads.push_back(output_quad);
	}

	return output_quads;
}

template<typename NextStage>
void neutral_tracer::icos_triangles(NextStage& ns, unsigned int tet_i) const
{
	using namespace detail;
	auto splitter_s1 = make_op<sing_splitter>(ns, 1, tets[tet_i]);
	auto splitter_s0 = make_op<sing_splitter>(splitter_s1, 0, tets[tet_i]);

	for (const auto& s_tri : neutral_sphere_tris)
	{
		neutral_surface_tri out;
		for (unsigned int j = 0; j < 3; j++)
		{
			out[j].tet = tet_i;
			out[j].mid_evec = neutral_sphere_verts[s_tri[j]];
		}
		splitter_s0(out);
	}

	splitter_s0.done();
}

template<typename NextStage>
void neutral_tracer::elliptic_quads(NextStage& ns, unsigned int tet_i) const
{
	float_type x_inc = M_PI / elliptic_x_cells;
	float_type y_inc = M_PI / elliptic_y_cells;

	const tet_internal& tet = tets[tet_i];
	float_type cokernel_min = tet.eigenvalues[0];
	float_type cokernel_mid = tet.eigenvalues[1];
	float_type cokernel_max = tet.eigenvalues[2];

	float_type f_sq_m1 =
		(cokernel_mid - cokernel_min) / (cokernel_max - cokernel_mid) - 1.0;

	std::vector<neutral_surface_vert> prev_row;
	float_type y = 0.0;
	for (unsigned int i = 0; i <= elliptic_y_cells; i++, y += y_inc)
	{
		std::vector<neutral_surface_vert> row;

		float_type x = 0.0;
		for (unsigned int j = 0; j < 2 * elliptic_x_cells; j++, x += x_inc)
		{
			Vector3ft mid = elliptic_param(f_sq_m1, Vector2ft(x, y));

			neutral_surface_vert vert;
			vert.tet = tet_i;
			vert.mid_evec = tet.eigenvectors * mid;

			// Mark singularities.
			if (i == 0                && j == 0 ||
			    i == elliptic_y_cells && j == elliptic_x_cells)
			{
				vert.kind = neutral_surface_vert::SINGULARITY;
				vert.index = 0;
				vert.bary = Vector4ft::Zero();
			}
			else if (i == 0                && j == elliptic_x_cells ||
			         i == elliptic_y_cells && j == 0)
			{
				vert.kind = neutral_surface_vert::SINGULARITY;
				vert.index = 1;
				vert.bary = Vector4ft::Zero();
			}

			row.push_back(vert);
		}

		if (i > 0)
		{
			unsigned int j_prev = 2 * elliptic_x_cells - 1;
			for (unsigned int j = 0; j < 2 * elliptic_x_cells; j++)
			{
				if (i != 1 && i != elliptic_y_cells || f_sq_m1 != -1.0)
				{
					ns({
						prev_row[j],
						prev_row[j_prev],
						row[j_prev],
						row[j]
					});
				}
				// TODO: Handle double singularities.
				else if (i == 1)
					ns({prev_row[0], prev_row[0], row[j_prev], row[j]});
				else
					ns({prev_row[j], prev_row[j_prev], row[elliptic_x_cells], row[elliptic_x_cells]});

				j_prev = j;
			}
		}

		std::swap(prev_row, row);
	}

	ns.done();
}

Vector3ft
neutral_tracer::elliptic_param(float_type f_sq_m1, const Vector2ft& p)
{
	float_type cos_x = cos(p.x());
	float_type sin_x = sin(p.x());
	float_type cos_y = cos(p.y());
	float_type sin_y = sin(p.y());

	Vector3ft mid(cos_x * sqrt(1.0 + f_sq_m1 * (cos_y * cos_y)),
	              sin_x * sin_y, cos_y);
	return mid.normalized();
}

Vector2ft
neutral_tracer::elliptic_inv_param(float_type f, float_type f_sq,
                                   const Vector3ft& m)
{
	std::complex<float_type> psi(m.x(), m.y());
	float_type f_z = f * m.z();
	float_type z_sq = m.z() * m.z();
	float_type f_sq_z_sq = f_sq * z_sq;

	// TODO: Is there a more efficient way of getting the correct branch cut
	// than splitting it into two square roots.
	std::complex<float_type> t =
		psi + std::sqrt(psi + f_z) * std::sqrt(psi - f_z);

	float_type theta = std::arg(-t) + M_PI;

	// std::norm(t) should always be bigger than f_sq_z_sq in exact arithmetic.
	float_type len_sq = std::max(std::norm(t), f_sq_z_sq);
	float_type phi = M_PI_2 -
		std::atan(2.0 * m.z() * std::sqrt(len_sq) / (len_sq - f_sq_z_sq));

	return Vector2ft(theta, phi);
}

template<typename NextStage>
void neutral_tracer::jacobi_elliptic_quads(
	NextStage& ns, unsigned int tet_i) const
{
	assert(0); // TODO
}

// Find the (barycentric) location for the tensor with the given medium
// eigenvector.
Vector4ft neutral_tracer::field_mid_to_loc(
	const Eigen::Matrix<float_type, 3, 4>& field_mid_mat)
{
	// Take the wedge product of the three rows to get the kernel.
	Vector4ft kernel;
	kernel[0] =  field_mid_mat.rightCols<3>().determinant();
	kernel[1] = -(Matrix3ft() << field_mid_mat.col(0),
	              field_mid_mat.rightCols<2>()).finished().determinant();
	kernel[2] =  (Matrix3ft() << field_mid_mat.leftCols<2>(),
	              field_mid_mat.col(3)).finished().determinant();
	kernel[3] = -field_mid_mat.leftCols<3>().determinant();
	return kernel;
}

// Find the barycentric coordinates of the location reached by approaching the
// singularity (field_mid_mat) from adjacent.
Vector4ft neutral_tracer::sing_loc(
	const Eigen::Matrix<float_type, 3, 4>& field_mid_mat,
	const Eigen::Matrix<float_type, 3, 4>& adjacent)
{
	Vector4ft result;
	for (unsigned int i = 0; i < 4; i++)
	{
		Matrix3ft submat_sing;
		Matrix3ft submat_adjacent;

		submat_sing << field_mid_mat.col((i + 1) % 4),
		               field_mid_mat.col((i + 2) % 4),
		               field_mid_mat.col((i + 3) % 4);
		submat_adjacent << adjacent.col((i + 1) % 4),
		                   adjacent.col((i + 2) % 4),
		                   adjacent.col((i + 3) % 4);

		Matrix3ft sing_adj;
		adjugate_3x3(submat_sing, sing_adj);
		float_type derivative = (sing_adj * submat_adjacent).trace();
		if (i % 2)
			derivative = -derivative;
		result[i] = derivative;
	}
	return result;
}

Eigen::Matrix<float_type, 3, 4>
neutral_tracer::get_field_mid_mat(const tet_internal& tet, Vector3ft mid)
{
	Eigen::Matrix<float_type, 3, 4> field_mid_mat;
	for (unsigned int i = 0; i < 4; i++)
		field_mid_mat.col(i) = tensor3_to_matrix(tet.field.col(i)) * mid;
	return field_mid_mat;
}

Vector4ft neutral_tracer::mid_evec_to_bary(
	const tet_internal& tet, Vector3ft mid)
{
	return field_mid_to_loc(get_field_mid_mat(tet, mid));
}

Vector3ft neutral_tracer::mid_evec_to_normal(
	const tet_internal& tet, Vector3ft mid)
{
	return mid_evec_to_normal(get_field_mid_mat(tet, mid), tet.loc_inv, mid);
}

Vector3ft neutral_tracer::mid_evec_to_normal(
	const Eigen::Matrix<float_type, 3, 4>& field_mid_mat,
	const Matrix3ft& loc_inv, Vector3ft mid)
{
	Eigen::Matrix<float_type, 1, 4> normal_bary =
		mid.transpose() * field_mid_mat;
	Eigen::Matrix<float_type, 1, 3> normal_bary3 =
		normal_bary.rightCols<3>() - normal_bary.col(0).replicate<1, 3>();

	return (normal_bary3 * loc_inv).transpose();
}

neutral_tracer::neutral_surface_vert neutral_tracer::intersect_face_sphere_edge(
	size_t tet_i, const tet_internal& tet, unsigned int face,
	const Eigen::Matrix<float_type, 5, 3>& face_field,
	const neutral_surface_vert* end_points[2])
{
	unsigned int num_sings = 0;
	for (unsigned int i = 0; i < 2; i++)
		if (end_points[i]->kind == neutral_surface_vert::SINGULARITY)
			num_sings++;

	if (num_sings == 0)
	{
		Vector3ft mids[2] = {end_points[0]->mid_evec, end_points[1]->mid_evec};
		return intersect_face_sphere_edge_no_sing(
			tet_i, tet, face, face_field, mids, end_points[0]->bary);
	}

	if (num_sings == 1)
	{
		unsigned int sing_v =
			(end_points[1]->kind == neutral_surface_vert::SINGULARITY);
		unsigned int normal_v = !sing_v;

		const neutral_surface_vert* sorted_endpoints[2] =
			{end_points[normal_v], end_points[sing_v]};
		auto intersection =
			intersect_face_sphere_edge_endpoint_root(
				tet_i, tet, face, face_field, sorted_endpoints);
		if (!intersection.second)
		{
			// Split was between singularities. TODO
			Vector4ft adj_bary[2];
			adj_bary[0] = end_points[sing_v]->bary;
			adj_bary[1] = sing_loc(
				get_field_mid_mat(tet, end_points[sing_v]->mid_evec),
				get_field_mid_mat(tet, end_points[normal_v]->mid_evec));
			return intersect_face_sing(
				tet_i, face, end_points[sing_v]->index,
				end_points[sing_v]->mid_evec, adj_bary);
		}

		return intersection.first;
	}
	else // num_sings == 2
	{
		Matrix3ft field_mid_mat[2];
		for (unsigned int i = 0; i < 3; i++)
			field_mid_mat[0].col(i) =
				tensor3_to_matrix(face_field.col(i)) * end_points[0]->mid_evec;

		// TODO: Make sure that there are no two adjacent singularities.

		Vector4ft adj_bary[2] = {end_points[0]->bary, end_points[1]->bary};
		return intersect_face_sing(
			tet_i, face, end_points[0]->index,
			end_points[0]->mid_evec, adj_bary);
	}
}

// 0 is the normal endpoint. 1 is the endpoint with a root.
std::pair<neutral_tracer::neutral_surface_vert, bool>
neutral_tracer::intersect_face_sphere_edge_endpoint_root(
	size_t tet_i, const tet_internal& tet,
	unsigned int face, const Eigen::Matrix<float_type, 5, 3>& face_field,
	const neutral_surface_vert* end_points[2])
{
	Matrix3ft field_mid_mat[2];
	for (unsigned int v = 0; v < 2; v++)
		for (unsigned int i = 0; i < 3; i++)
			field_mid_mat[v].col(i) =
				tensor3_to_matrix(face_field.col(i)) * end_points[v]->mid_evec;

	// Adjust for alternating + and - in field_mid_to_loc
	if (face % 2)
		for (unsigned int vert = 0; vert < 2; vert++)
			field_mid_mat[vert] = -field_mid_mat[vert];

	Matrix3ft adj[2];
	adjugate_3x3(field_mid_mat[1], adj[1]);
	float_type root_term = (field_mid_mat[0] * adj[1]).trace();

	if (!((root_term >= 0.0) ^ (end_points[0]->bary[face] >= 0.0)))
		return {neutral_surface_vert(), false};

	adjugate_3x3(field_mid_mat[0], adj[0]);
	float_type mixed_term = (field_mid_mat[1] * adj[0]).trace();

	auto solutions = quadratic::real_roots_assume_exist(
		end_points[0]->bary[face], 0.5 * mixed_term, root_term);

	// Pick the closest
	neutral_surface_vert closest;
	float_type closest_dist = std::numeric_limits<float_type>::infinity();
	for (unsigned int i = 0; i < 2; i++)
	{
		Vector2ft solution = solutions.second.col(i);
		float_type sum = solution.sum();
		if (sum < 0.0)
		{
			solution = -solution;
			sum = -sum;
		}

		// Figure out how far it is from the edge midpoint. Twice the distance
		// that would be measured if mid[0] is at 0 and mid[1] is at 1.
		float_type dist = std::abs(solution[0] - solution[1]) / sum;
		if (dist < closest_dist)
		{
			closest_dist = dist;
			closest.mid_evec =
				(solution[0] * end_points[0]->mid_evec +
				 solution[1] * end_points[1]->mid_evec).normalized();
		}
	}
	closest.bary = mid_evec_to_bary(tet, closest.mid_evec);
	closest.bary[face] = 0;
	closest.tet = tet_i;
	return {closest, true};
}

// Gives the location between the two adj_bary's that intersects face.
neutral_tracer::neutral_surface_vert neutral_tracer::intersect_face_sing(
	size_t tet_i, unsigned int face, unsigned int index, Vector3ft mid_evec,
	Vector4ft adj_bary[2])
{
	Vector2ft solution(-adj_bary[1][face], adj_bary[0][face]);
	if (solution.sum() < 0.0)
		solution = -solution;

	neutral_surface_vert out;
	out.mid_evec = mid_evec;
	out.bary = solution.x() * adj_bary[0] + solution.y() * adj_bary[1];
	out.bary[face] = 0;
	out.tet = tet_i;
	out.kind = neutral_surface_vert::SINGULARITY;
	out.index = index;
	return out;
}

neutral_tracer::neutral_surface_vert
neutral_tracer::intersect_face_sphere_edge_no_sing(
	size_t tet_i, const tet_internal& tet, unsigned int face,
	const Eigen::Matrix<float_type, 5, 3>& face_field, Vector3ft mid[2],
	const Vector4ft& bary_nearby)
{
	Matrix3ft field_mid_mat[2];
	for (unsigned int vert = 0; vert < 2; vert++)
		for (unsigned int i = 0; i < 3; i++)
			field_mid_mat[vert].col(i) =
				tensor3_to_matrix(face_field.col(i)) * mid[vert];
	Eigen::GeneralizedEigenSolver<Matrix3ft>
		solver(field_mid_mat[0], field_mid_mat[1]);

	neutral_surface_vert closest;
	unsigned int closest_i;
	float_type closest_dist = std::numeric_limits<float_type>::infinity();
	for (unsigned int i = 0; i < 3; i++)
	{
		std::complex<double> alpha = solver.alphas()[i];
		double beta = solver.betas()[i];

		// Don't bother with complex roots.
		if (alpha.imag() != 0.0)
			continue;

		// det(beta*A + -alpha*B) = 0
		Vector2ft solution(beta, -alpha.real());
		float_type sum = solution.sum();
		if (sum < 0.0)
		{
			solution = -solution;
			sum = -sum;
		}

		// Figure out how far it is from the edge midpoint. Twice the distance
		// that would be measured if mid[0] is at 0 and mid[1] is at 1.
		float_type dist = std::abs(solution[0] - solution[1]) / sum;
		if (dist < closest_dist)
		{
			closest_dist = dist;
			closest_i = i;
			closest.mid_evec =
				(solution[0] * mid[0] + solution[1] * mid[1]).normalized();
		}
	}

	closest.bary = mid_evec_to_bary(tet, closest.mid_evec);
	closest.bary[face] = 0;
	closest.tet = tet_i;
	return closest;
}

std::array<std::vector<Vector3ft_na>, 2>
neutral_tracer::sample_face_intersection(size_t tet_i, unsigned int face)
{
	tet_internal& tet = tets[tet_i];
	tensor3 cokernel = tet.fieldQR.householderQ() * tensor3::Unit(4);
	Matrix3ft cokernel_quad = tensor3_to_quadratic_eq(cokernel);

	// tensor3_to_quadratic_eq + matrix_to_tensor3 aren't inverses. Combining
	// them is like multiplying by the metric, so that
	// tensor3_to_quadratic_eq(face_cokernel) and cokernel_quad are orthogonal
	// by the Frobenius inner product.
	Eigen::Matrix<float_type, 5, 4> face_field_and_cokernel = tet.field;
	face_field_and_cokernel.col(face) = matrix_to_tensor3(cokernel_quad);
	tensor3 face_cokernel =
		face_field_and_cokernel.colPivHouseholderQr().householderQ() *
		tensor3::Unit(4);

	std::array<std::vector<Vector3ft_na>, 2> samples;

	const size_t num_samples = 1000;
	auto prev_q = Eigen::Quaternion<float_type>::Identity();
	for (size_t i = 0; i < num_samples; i++)
	{
		float_type theta = M_PI * i / (num_samples - 1);
		tensor3 comb_cokernel =
			cos(theta) * cokernel + sin(theta) * face_cokernel;

		Eigen::SelfAdjointEigenSolver<Matrix3ft>
			eig(tensor3_to_quadratic_eq(comb_cokernel));
		Matrix3ft eigenvectors = eig.eigenvectors();
		if (eigenvectors.determinant() < 0.0)
			eigenvectors = -eigenvectors;

		Eigen::Quaternion<float_type> q(eigenvectors);
		q = q * quaternion_group::closest_in_left_coset(q, prev_q)
			.to_eigen<float_type>();
		prev_q = q;

		// Find the two points, which are the singularities of comb_cokernel.

		float_type min = eig.eigenvalues()[0];
		float_type mid = eig.eigenvalues()[1];
		float_type max = eig.eigenvalues()[2];

		Vector2ft normal0 =
			Vector2ft(mid - min, max - mid).array().sqrt() / sqrt(max - min);
		samples[0].push_back(q * Vector3ft(normal0[0], 0.0, normal0[1]));
		samples[1].push_back(q * Vector3ft(normal0[0], 0.0, -normal0[1]));
	}

	return samples;
}

void neutral_tracer::render(const Matrix4gf& mvp)
{
	glUseProgram(neutral_shader_program);
	glUniformMatrix4fv(neutral_MVP_uniform, 1, GL_FALSE, mvp.data());

	glEnable(GL_CLIP_DISTANCE0);
	glEnable(GL_CLIP_DISTANCE1);
	glEnable(GL_CLIP_DISTANCE2);
	glEnable(GL_CLIP_DISTANCE3);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glLineWidth(2.0f);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, lighting_buffer);

	glBindVertexArray(neutral_vao);
	glDrawArrays(GL_TRIANGLES, 0, 3 * neutral_surface_tri_count);
	glBindVertexArray(0);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);

	glDisable(GL_CLIP_DISTANCE0);
	glDisable(GL_CLIP_DISTANCE1);
	glDisable(GL_CLIP_DISTANCE2);
	glDisable(GL_CLIP_DISTANCE3);

	glUseProgram(0);
}

void neutral_tracer::create_neutral_sphere(unsigned int subdivisions)
{
	const double icosX = 0.525731112119133606025669084847876607285497932243341781528;
	const double icosY = 0.850650808352039932181540497063011072240401403764816881836;

	// v0 and its 5 vertex neighborhood.
	neutral_sphere_verts.emplace_back(     0,  icosX, icosY);
	neutral_sphere_verts.emplace_back(     0, -icosX, icosY);
	neutral_sphere_verts.emplace_back( icosY,      0, icosX);
	neutral_sphere_verts.emplace_back(-icosY,      0, icosX);
	neutral_sphere_verts.emplace_back( icosX,  icosY,     0);
	neutral_sphere_verts.emplace_back(-icosX,  icosY,     0);

	// Mirrored on opposite side.
	for (int i = 0; i < 6; i++)
		neutral_sphere_verts.push_back(-neutral_sphere_verts[i]);

	// v0's neighborhood.
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{0, 5, 3});
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{0, 3, 1});
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{0, 1, 2});
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{0, 2, 4});
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{0, 4, 5});

	// One more ring.
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{1, 3, 10});
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{3, 5,  8});
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{5, 4,  7});
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{4, 2,  9});
	neutral_sphere_tris.emplace_back(std::array<size_t, 3>{2, 1, 11});

	// Mirror on opposite side.
	for (int i = 0; i < 10; i++)
	{
		const auto& tri = neutral_sphere_tris[i];
		neutral_sphere_tris.emplace_back(std::array<size_t, 3>
			{(tri[1] + 6) % 12, (tri[0] + 6) % 12, (tri[2] + 6) % 12});
	}

	// Subdivide
	for (int i = 0; i < subdivisions; i++)
	{
		std::map<std::pair<size_t, size_t>, size_t> subdivided_edges;

		std::vector<std::array<size_t, 3>> prev_tris;
		std::swap(prev_tris, neutral_sphere_tris);
		for (const auto& tri : prev_tris)
		{
			size_t split_verts[3];
			for (int j = 0; j < 3; j++)
			{
				size_t v0 = tri[(j + 1) % 3];
				size_t v1 = tri[(j + 2) % 3];
				if (v0 > v1)
					std::swap(v0, v1);

				size_t split_v = neutral_sphere_verts.size();
				auto insert_res =
					subdivided_edges.insert(
						std::make_pair(std::make_pair(v0, v1), split_v));
				if (insert_res.second)
				{
					Vector3ft_na v0_evec = neutral_sphere_verts[v0];
					Vector3ft_na v1_evec = neutral_sphere_verts[v1];
					Vector3ft_na split_evec = (v0_evec + v1_evec).normalized();
					neutral_sphere_verts.push_back(split_evec);
				}
				else
					split_v = insert_res.first->second;

				split_verts[j] = split_v;
			}
			neutral_sphere_tris.emplace_back(std::array<size_t, 3>
				{tri[0], split_verts[2], split_verts[1]});
			neutral_sphere_tris.emplace_back(std::array<size_t, 3>
				{tri[1], split_verts[0], split_verts[2]});
			neutral_sphere_tris.emplace_back(std::array<size_t, 3>
				{tri[2], split_verts[1], split_verts[0]});
			neutral_sphere_tris.emplace_back(std::array<size_t, 3>
				{split_verts[0], split_verts[1], split_verts[2]});
		}
	}
}

// Compute the adjugate of a matrix. Mostly copied from Eigen's 3x3 inverse.
template<typename MatrixType, int i, int j>
static inline typename MatrixType::Scalar cofactor_3x3(const MatrixType& m)
{
	enum {
		i1 = (i+1) % 3,
		i2 = (i+2) % 3,
		j1 = (j+1) % 3,
		j2 = (j+2) % 3
	};
	return m.coeff(i1, j1) * m.coeff(i2, j2)
	     - m.coeff(i1, j2) * m.coeff(i2, j1);
}

template<typename MatrixType, typename ResultType>
static inline void adjugate_3x3(const MatrixType& matrix, ResultType& result)
{
	typedef typename ResultType::Scalar Scalar;
	Eigen::Matrix<typename MatrixType::Scalar,3,1> cofactors_col0;
	result.coeffRef(0,0) = cofactor_3x3<MatrixType,0,0>(matrix);
	result.coeffRef(0,1) = cofactor_3x3<MatrixType,1,0>(matrix);
	result.coeffRef(0,2) = cofactor_3x3<MatrixType,2,0>(matrix);
	result.coeffRef(1,0) = cofactor_3x3<MatrixType,0,1>(matrix);
	result.coeffRef(1,1) = cofactor_3x3<MatrixType,1,1>(matrix);
	result.coeffRef(1,2) = cofactor_3x3<MatrixType,2,1>(matrix);
	result.coeffRef(2,0) = cofactor_3x3<MatrixType,0,2>(matrix);
	result.coeffRef(2,1) = cofactor_3x3<MatrixType,1,2>(matrix);
	result.coeffRef(2,2) = cofactor_3x3<MatrixType,2,2>(matrix);
}
