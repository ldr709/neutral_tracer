#version 430

uniform mat4 MVP;

layout(std140) uniform lightBuf
{
	vec4 uLightPos;
	vec4 uLightSpecularColor;
	vec4 uEyePos;
	float uKa;
	float uKd;
	float uKs;
	float uShininess;
} Light;

layout(location = 0) in vec4 loc;
layout(location = 1) in vec4 bary_loc;
layout(location = 2) in vec3 normal_attr;

out vec3 normal;
out vec3 light;
out vec3 eye;
out vec4 bary_frag;

void main()
{
	vec4 loc = loc;
	vec3 normal_attr = normal_attr;
	if (false)//loc.w < 0)
	{
		loc = -loc;
		normal_attr = -normal_attr;
	}

	normal = normal_attr;
	gl_Position = MVP * loc;

	light = loc.w * Light.uLightPos.xyz - Light.uLightPos.w * loc.xyz;
	eye   = loc.w * Light.uEyePos.xyz   - Light.uEyePos.w * loc.xyz;

	gl_ClipDistance[0] = 1.0;//bary_loc[0];
	gl_ClipDistance[1] = 1.0;//bary_loc[1];
	gl_ClipDistance[2] = 1.0;//bary_loc[2];
	gl_ClipDistance[3] = 1.0;//bary_loc[3];

	bary_frag = abs(bary_loc);
}
