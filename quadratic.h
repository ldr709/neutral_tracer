#include <algorithm>
#include <cmath>
#include <utility>
#include <Eigen/Dense>

namespace quadratic
{

namespace detail
{

using namespace Eigen;

template<typename Scalar>
std::pair<unsigned int, Matrix<Scalar, 2, 2>>
solver_impl(Scalar a, Scalar b, Scalar c, Scalar discrim)
{
	Matrix<Scalar, 2, 2> output = Matrix<Scalar, 2, 2>::Zero();
	if (discrim < 0.0)
		return std::make_pair(0, output);

	Scalar sqrt_part = std::copysign(sqrt(discrim), b);
	Scalar b_sqrt_part = b + sqrt_part;

	// If b is too small, use a different method to prevent outputting (0, 0)
	// when a or c is zero.
	if (b_sqrt_part == sqrt_part)
	{
		if (std::abs(a) >= std::abs(c))
		{
			output.col(0) << -sqrt_part, a;
			output.col(1) << sqrt_part, a;
		}
		else
		{
			output.col(0) << c, -sqrt_part;
			output.col(1) << c, sqrt_part;
		}
	}
	else
	{
		output.col(0) << -b_sqrt_part, a;
		output.col(1) << c, -b_sqrt_part;
	}

	return std::make_pair(2, output);
}

// Solves a 2x2 generalized eigenvalue problem using the quadratic equation.
// Mostly from "How to Solve a Quadratic Equation" by James F. Blinn, but also
// from the cubic equation part of that document. det(x A + y B) = 0. Finds one
// (x, y) pair on each real line of solutions, together with the number of real
// solutions. Roots are packed into the returned matrix in columns. Returned
// roots in no particular order.
template<typename D0>
std::pair<unsigned int, Matrix<typename MatrixBase<D0>::Scalar, 2, 2>>
general_eigen(const MatrixBase<D0>& A, const MatrixBase<D0>& B)
{
	EIGEN_STATIC_ASSERT_MATRIX_SPECIFIC_SIZE(D0, 2, 2);

	typedef typename MatrixBase<D0>::Scalar Scalar;
	Matrix<Scalar, 2, 2> T;
	T <<  A(1, 1), -A(0, 1),
	     -A(1, 0),  A(0, 0);

	// a = det(A)
	// b = tr(adj(A) B) / 2
	// c = det(B)
	// Want to find b^2 - a c, but with better stability. For the purpose of
	// algebraic manipulation, divide through by det(A). U = A^-1 B. a' = 1.
	// b' = tr(U)/2. c' = det(U). Let l0, l1 be U's eigenvalues. 2 b' = l0 + l1.
	// c' = l0 l1. b'*b' - a'*c' = (l0^2 + l1^2)/4 + l0 l1 / 2 - l0 l1
	//                           = (l0^2 + l1^2)/4 - l0 l1 / 2.
	//                           = (l0 - l1)^2 / 4
	//                           = -(l0 - (l0 + l1)/2) * (l1 - (l0 + l1)/2)
	//                           = -det(U - I tr(U)/2)
	// b^2 - a c = det(A)^2 (b'^2 - a' c') = -det(det(A) (U - I tr(U)/2))
	//           = -det(adj(A) B - I tr(adj(A) B) / 2)
	T *= B;
	Scalar b = Scalar(0.5) * T.trace();
	T.diagonal() -= Matrix<Scalar, 2, 1>::Constant(b);
	Scalar discrim = -T.determinant();

	return solver_impl(A.determinant(), b, B.determinant(), discrim);
}

// Find the real eigenvalues of the matrix A. Return how many there are,
// together with the values. The values are in the collumns of a matrix, in the
// form x / y. Essentially general_eigen specialized to A = -I.
template<typename D0>
std::pair<unsigned int, Matrix<typename MatrixBase<D0>::Scalar, 2, 2>>
eigen(const MatrixBase<D0>& A)
{
	EIGEN_STATIC_ASSERT_MATRIX_SPECIFIC_SIZE(D0, 2, 2);

	typedef typename MatrixBase<D0>::Scalar Scalar;
	Eigen::Matrix<Scalar, 2, 2> T = -A;
	Scalar b = Scalar(0.5) * T.trace();
	T.diagonal() -= Matrix<Scalar, 2, 1>::Constant(b);
	Scalar discrim = -T.determinant();

	return solver_impl(Scalar(1), b, A.determinant(), discrim);
}

// Solve a x^2 + 2 b x y + c y^2 = 0. Finds one (x, y) pair on each line of
// solutions. Similar to previous.
template<typename Scalar>
std::pair<unsigned int, Matrix<Scalar, 2, 2>>
real_roots(Scalar a, Scalar b, Scalar c)
{
	// Could also be implemented using general_eigen:
	// Matrix<Scalar, 2, 2> A, B;
	// A <<  1, b,
	//       0, a;
	// B <<  0, c,
	//      -1, b;
	// return general_eigen(A, B);

	Scalar discrim = b * b - a * c;
	return solver_impl(a, b, c, discrim);
}

// Same as previous, except will return roots no matter what -- even if they
// would normally be complex.
template<typename Scalar>
std::pair<unsigned int, Matrix<Scalar, 2, 2>>
real_roots_assume_exist(Scalar a, Scalar b, Scalar c)
{
	// Could also be implemented using general_eigen:
	// Matrix<Scalar, 2, 2> A, B;
	// A <<  1, b,
	//       0, a;
	// B <<  0, c,
	//      -1, b;
	// return general_eigen(A, B);

	Scalar discrim = std::max(b*b - a * c, 0.0);
	return solver_impl(a, b, c, discrim);
}

// Solves [x y] A [x] = 0. Works similarly to previous. Only looks at A's upper
//                [y]
// triangular part (assumes that it is symmetric). Only real roots.
template<typename D0>
std::pair<unsigned int, Matrix<typename MatrixBase<D0>::Scalar, 2, 2>>
real_roots(const MatrixBase<D0>& A)
{
	EIGEN_STATIC_ASSERT_MATRIX_SPECIFIC_SIZE(D0, 2, 2);
	return quadratic_real_roots(A(0, 0), A(0, 1), A(1, 1));
}

}

using detail::general_eigen;
using detail::real_roots;
using detail::real_roots_assume_exist;

}
