#ifndef TENSOR_LINES_NEUTRAL_PARAM_H
#define TENSOR_LINES_NEUTRAL_PARAM_H

#include <array>
#include <iterator>
#include <map>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/StdVector>
#include "gl_utils.h"
#include "tensor.h"
#include "neutral.h"

// neutral tracer based on parameterizing faces.
class neutral_tracer_param
{
	typedef Eigen::ColPivHouseholderQR
		<Eigen::Matrix<float_type, 5, 4>> fieldQR_t;

public:
	typedef degenerate_curve_tracer::vertex vertex;
	typedef degenerate_curve_tracer::face face;
	typedef degenerate_curve_tracer::tet tet;

	const std::vector<vertex>& vertices;
	const std::vector<face>& faces;
	const std::vector<tet>& tets;

	enum neutral_vert_kind
	{
		NORMAL,
		SINGULARITY, // Note that SINGULARITIES will often be on tet faces.
		TETEDGE,
		TETFACE,
	} kind;

	struct neutral_vert
	{
		Vector3ft loc;
		Vector3ft mid;
		Vector3ft normal = Vector3ft::Zero();
		neutral_vert_kind kind = NORMAL;

		// If NORMAL, index of tet.
		// if SINGULARITY or TETFACE, index of face.
		// if TETEDGE, index of the two adjacent vertices, in sorted order.
		//
		// If kind != TETEDGE then don't use index[1].
		size_t index[2];
	};
	std::vector<neutral_vert, Eigen::aligned_allocator<neutral_vert>>
		neutral_verts;

	struct neutral_triangle
	{
		size_t neutral_v[3];
	};
	std::vector<neutral_triangle> neutral_triangles;

	struct light_buf
	{
		float uLightPos[4];
		float uLightSpecularColor[4];
		float uEyePos[4];
		float uKa;
		float uKd;
		float uKs;
		float uShininess;
	};

	Eigen::Matrix<float_type, 3, 4>
	get_field_mid_mat(const tet& tet, const Vector3ft& mid) const;
	bool tet_mid_has_wrong_sign(const tet& tet, const Vector3ft& mid) const;

private:
	struct edge
		// Increasing order.
		: public std::array<size_t, 2>
	{
		// Puts v0 and v1 into correct order.
		inline edge(size_t v0, size_t v1);
	};

	struct neutral_vert_tetedge
	{
		size_t neutral_v;
		Vector2ft bary;
	};

	struct neutral_vert_tetface
	{
		size_t neutral_v;
		Vector3ft bary;

		// If SINGULARITY, low bit gives index of singularity, next bit
		// specifies the tet.
		// Otherwise this means nothing.
		unsigned int flag = 0;
	};

	struct neutral_vert_tet
	{
		// If this is a singularity, there are multiple neutral_vs, and this
		// stores the singularity number instead. Otherwise, the second
		// neutral_v will be ((size_t) -1).
		size_t neutral_v[2];

		// tet_verts index of next vertex on boundary after this singularity, if
		// this is a singularity.
		size_t next_v;
	};

	struct tet_info
	{
		Eigen::Matrix<float_type, 3, 3> loc_inv;
		Eigen::Matrix<float_type, 3, 3> eigenvectors;
		Vector3ft eigenvalues;

		fieldQR_t fieldQR;

		// Cokernel of field, represented as two vectors a, b such that the
		// cokernel is (a b^t f b a^t) / 2 - (a^t b / 3) I
		Vector3ft sings[2];

		std::vector<size_t> boundary_ends;
		std::vector<neutral_vert_tet> tet_verts;
	};
	std::vector<tet_info, Eigen::aligned_allocator<tet_info>> tet_infos;

	struct edge_info
	{
		neutral_vert_tetedge intersections[3];
		unsigned int num_intersections = 0;
	};
	std::map<edge, edge_info, std::less<edge>,
		Eigen::aligned_allocator<std::pair<edge, edge_info>>> edge_infos;

	struct face_info
	{
		size_t tet[2];
		unsigned int num_tets = 0;

		typedef std::vector<neutral_vert_tetface,
			Eigen::aligned_allocator<neutral_vert_tetface>> trace;

		// Each trace is inside the face and goes from a tetedge or singularity
		// to the next one.
		std::vector<trace> intersections;
	};
	std::vector<face_info, Eigen::aligned_allocator<face_info>> face_infos;

	const size_t boundary_samples;
	const size_t elliptic_x_cells;
	const size_t elliptic_y_cells;

	Eigen::Matrix<float_type, 3, 4> get_tet_loc(const tet& t);
	Eigen::Matrix<float_type, 5, 4> get_tet_field(const tet& t);

	Eigen::Matrix<float_type, 3, 3> get_face_loc(const face& t);
	Eigen::Matrix<float_type, 5, 3> get_face_field(const face& t);

	Eigen::Matrix<float_type, 3, 2> get_edge_loc(const edge& t);
	Eigen::Matrix<float_type, 5, 2> get_edge_field(const edge& t);

	Vector3ft face_mid_to_bary(const face& f, const Vector3ft& mid) const;
	Vector4ft tet_mid_to_bary(const tet& t, const Vector3ft& mid) const;
	Vector3ft mid_evec_to_normal(const tet& t, const tet_info& ti,
	                             const Vector3ft& mid) const;

	static Vector2ft to_stereographic(const Vector3ft& x);
	static Vector3ft from_stereographic(const Vector2ft& x);

	struct face_param_starting_vert
	{
		neutral_vert_tetface v[2];
		bool present[2];
		float_type theta;
	};

	// Get the two points (medium eigenvectors) of the parametrization at theta.
	//
	// q should be the quaternion representation of the eigenvectors of a
	// nearby cokernel, used to maintain consistency in the order of the two
	// points this function outputs. If you don't need that consistency pass the
	// identity quaternion. q will be updated to hold the eigenvectors of the
	// new cokernel.
	static Eigen::Matrix<float_type, 3, 2> sample_face_param(
		const Eigen::Matrix<float_type, 5, 2>& cokernel,
		Eigen::Quaternion<float_type>& q,
		float_type theta);
	void reorder_face_sample(face_param_starting_vert& sv,
	                         const Eigen::Matrix<float_type, 3, 2>& sample);
	void compute_face_info(size_t face_index, const face& f, face_info& fi);

	void find_tet_boundaries(size_t tet_index, const tet& t, tet_info& ti);
	bool traces_fit(
		const tet& tet, size_t tet_index,
		const face_info::trace& t0, const face_info& t0_face_info,
		bool reversed0,
		const face_info::trace& t1, const face_info& t1_face_info,
		bool reversed1);
	bool tet_face_actual_sing(
		size_t tet_index, const face_info& fi, const neutral_vert_tetface& v);
	void add_interior_verts(
		size_t tet_index, const tet& t, tet_info& ti, float_type f,
		float_type f_sq,
		const Matrix3ft& rot, const std::vector<Vector3ft_na>& tet_mids,
		std::vector<Vector2ft_na>& points_out,
		const std::array<size_t, 3>& tri);
	void sample_elliptic_triangle(
		size_t tet_index, const tet& t, tet_info& ti, float_type f_sq,
		const Matrix3ft& rot, std::vector<Vector2ft_na>& points_out,
		Vector2ft (&tri)[3]);

	struct neutral_surface_vattrs
	{
		GLfloat loc[4];
		GLfloat mid[4];
		GLfloat normal[3];
	};

	GLuint neutral_surface_buffer;
	GLuint neutral_index_buffer;
	GLuint lighting_buffer;

	static const GLuint neutral_loc_attr = neutral_tracer::neutral_loc_attr;
	static const GLuint neutral_mid_attr = neutral_tracer::neutral_bary_attr;
	static const GLuint neutral_normal_attr =
		neutral_tracer::neutral_normal_attr;
	GLint neutral_MVP_uniform;
	GLint neutral_lighting_uniform;

	GLuint neutral_shader_program;
	GLuint neutral_vao;

	void init_gl(const char* shader_prefix);

public:
	neutral_tracer_param(const char* shader_prefix,
	                     const std::vector<vertex>& vertices,
	                     const std::vector<face>& faces,
	                     const std::vector<tet>& tet,
	                     size_t boundary_samples,
	                     size_t elliptic_x_cells,
	                     size_t elliptic_y_cells);
	~neutral_tracer_param();

	light_buf get_lighting();
	void set_lighting(light_buf lighting);
	void render(const Matrix4gf& mvp, bool wireframe = false);
};

inline neutral_tracer_param::edge::edge(size_t v0, size_t v1)
{
	if (v0 > v1)
		std::swap(v0, v1);
	(*this)[0] = v0;
	(*this)[1] = v1;
}

// Gets a kernel that is know to be Dim dimensional.
template<unsigned int Dim, typename Derived>
Eigen::Matrix<typename Derived::Scalar, Derived::ColsAtCompileTime, Dim>
get_kernel(const Eigen::MatrixBase<Derived>& m)
{
	eigen_assert(m.rows() <= m.cols());
	typedef Eigen::Matrix<
		typename Derived::Scalar, Derived::ColsAtCompileTime,
		Derived::ColsAtCompileTime> Mat;

	// Relies on undocumented feature that ColPivHouseholderQR always orders R
	// such that the diagonal's absolute value is decreasing.
	return m.transpose().colPivHouseholderQr().householderQ() *
		Mat::Identity().template rightCols<Dim>();
}

// Not exactly distance, but is monotonic in distance. Inputs should be
// normalized.
inline float_type rp2_dist(const Vector3ft& a, const Vector3ft& b)
{
	Vector3ft x = a;
	if (x.dot(b) < 0.0)
		x = -x;
	return (x - b).squaredNorm();
}

#endif
