#include "tensor.h"
#include "neutral.h"
#include "quadratic.h"
#include "qr.h"

#include <algorithm>
#include <bitset>
#include <fstream>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <utility>

std::vector<degen_point> known_degen_points;
std::vector<degen_line_segment> known_degen_lines;

namespace
{

typedef Eigen::ColPivHouseholderQR<affine_field<3> > field_QR_type;
typedef Eigen::ColPivHouseholderQR<affine_field<2> > field_plane_QR_type;
typedef Eigen::SelfAdjointEigenSolver<Matrix3ft> field_eig_type;

// Initial tetrahedron or plan intersections used to tell where to trace.
struct initial_degen_point
{
	degen_point pnt;

	// Position on the ellipse in the veronese manifold.
	float_type theta;

	degenerate_curve_tracer::degen_curve_head* curve;
	bool entering_tet;
};

struct veronese_ellipse
{
	const Matrix3ft& cokernel;

	// Eigendecomposition for the cokernel tensor. This will give be used as a
	// basis for the non-degenerate eigenvector. Eigenvalues are in increasing
	// order.
	field_eig_type cokernel_eigen;

	// Project the Veronese manifold (RP2) onto the plane orthogonal to the
	// minor eigenvector. This will produce an ellipse aligned to the medium and
	// major eigenvectors. The sizes in the two directions are
	// sqrt(-lambda1 / lambda2) and sqrt(-lambda1 / lambda3), where
	// lambda1 < 0 <= lambda2 <= lambda3. Use 1/square of axises for stability.
	float_type semi_major_axis_inv_sq;
	float_type semi_minor_axis_inv_sq;

	// Assumes that cokernel is a symmetric, traceless, planar (or at least not
	// linear) tensor.
	veronese_ellipse(const Matrix3ft& cokernel);

	// Return the non-degenerate eigenvector of the point on the ellipse given
	// by angle.
	Vector3ft sample(float_type angle);

	// Same as sample, but uses cokernel's eigenvectors as a basis.
	Vector3ft sample_eigenspace(float_type angle);

	// Return the angle of nondegen, a point on the ellipse.
	float_type get_angle(const Vector3ft& nondegen);
	float_type get_angle_eigenspace(Vector3ft nondegen);

	Vector3ft get_tangent(const Vector3ft& nondegen);
	Vector3ft get_tangent_eigenspace(const Vector3ft& nondegen);
};

// Used to sort the initial degenerate points in the order they appear on the
// ellipse.
struct initial_degen_point_ellipse_order
{
	bool operator()(const initial_degen_point& a, const initial_degen_point& b)
	{
		return a.theta < b.theta;
	}
};

}

static void solve_homogeneous_quadratic(Matrix3ft quad1, Matrix3ft quad2,
                                        Vector3ft_na solutions[4],
                                        int& num_solutions);
static void solve_homogeneous_quadratic_helper_line_intersection
	(const Matrix3ft& quad, const Vector2ft& line, bool upperLeftRoot, int col,
	 const Matrix3ft& to_global, Vector3ft_na solutions[2], int& num_solutions);

static void
find_infinity_points(const affine_field<3>& field,
                     std::vector<initial_degen_point>& output_points);

static void
find_min_piv_points(const affine_field<3>& field, const field_QR_type& field_QR,
                    const Matrix3ft& cokernel,
                    std::vector<initial_degen_point>& output_points);

static void find_degenerates_kernel0(const affine_field<3>& field,
                                     const field_QR_type& field_QR,
                                     const Matrix3ft& cokernel);
static void find_degenerates_kernel1(const affine_field<3>& field,
                                     const field_QR_type& field_QR,
                                     const Matrix3ft& cokernel);

std::vector<degenerate_curve_tracer::degen_curve>
degenerate_curve_tracer::trace_curves()
{
	degenerate_curves.clear();

	face_ints.resize(faces.size());

	std::unique_ptr<std::vector<degen_curve_head*>[]> degen_curves_head_parts;
	const size_t num_faces = faces.size();

	#pragma omp parallel
	{
		#pragma omp single
		degen_curves_head_parts.reset(
			new std::vector<degen_curve_head*>[omp_get_num_threads()]());

		#pragma omp for
		for (int face = 0; face < num_faces; face++)
			find_face_degenerates(
				face, degen_curves_head_parts[omp_get_thread_num()]);

		#pragma omp single
		for (int i = 0; i < omp_get_num_threads(); i++)
			degenerate_curves.insert(
				degenerate_curves.end(),
				degen_curves_head_parts[i].begin(),
				degen_curves_head_parts[i].end());
	}
	degen_curves_head_parts.reset();

	for (size_t tet = 0; tet < tets.size(); tet++)
		find_tet_degenerates(tet);

	std::vector<degen_curve> out;
	std::vector<degen_curve_head*> to_free;
	for (size_t i = 0; i < degenerate_curves.size(); i++)
	{
		degen_curve_head* head = degenerate_curves[i];
		degen_curve_head* start = head;
		if (head->visited)
			continue;

		// Find the starting point.
		degen_curve_head* prev = NULL;
		do
		{
			advance_head(head, prev);
		} while (head && head != start);

		// Go the other way.
		std::swap(head, prev);
		start = head;

		assert(!head->visited);

		degen_curve curve;
		curve.second = (prev != NULL);
		do
		{
			curve.first.push_back(head->pnt);
			to_free.push_back(head);
			assert(!head->visited);
			head->visited = true;

			advance_head(head, prev);
		}
		while (head && head != start);

		out.push_back(curve);
	}
	degenerate_curves.clear();

	for (auto p : to_free)
		delete p;

	return out;
}

void degenerate_curve_tracer::find_face_degenerates(
	size_t face_i, std::vector<degen_curve_head*>& degen_head_current)
{
	face_internal face = get_face(face_i);
	field_plane_QR_type field_QR(face.field);

	Matrix3ft cokernel_quad[2];
	for (unsigned int i = 0; i < 2; i++)
	{
		tensor3 cokernel =
			field_QR.householderQ() * tensor3::Unit(tensor_size - 2 + i);
		cokernel_quad[i] = tensor3_to_quadratic_eq(cokernel);
	}

	int num_solutions;
	Vector3ft_na solutions[4];
	solve_homogeneous_quadratic(cokernel_quad[0], cokernel_quad[1],
	                            solutions, num_solutions);

	face_ints_info intersections;
	for (int i = 0; i < num_solutions; i++)
	{
		Vector3ft nondegen_vec = solutions[i].normalized();

		Vector3ft bary = field_QR.solve(
			matrix_trace_to_tensor3(nondegen_vec * nondegen_vec.transpose()));

		// Make sure it is inside the face.
		if ((bary.array() < 0.0).any() && (bary.array() > 0.0).any())
			continue;

		degen_point pnt{
			Eigen::Matrix<float_type, 4, 3>(
				face.loc.colwise().homogeneous()) * bary,
			nondegen_vec, face_i, true};

		degen_curve_head* head = new degen_curve_head();
		head->pnt = pnt;
		head->link[0] = NULL;
		head->link[1] = NULL;
		head->visited = false;
		degen_head_current.push_back(head);
		intersections.emplace_back(degen_head_current.back());
	}
	face_ints[face_i] = std::move(intersections);
}

void degenerate_curve_tracer::find_tet_degenerates(size_t tet_i)
{
	tet_internal tet = get_tet(tet_i);

	// Initial tetrahedron, infinity, or plane intersections used to tell
	// where to trace.
	std::vector<initial_degen_point> initial_degen_points;
	for (unsigned int i = 0; i < 4; i++)
	{
		auto& ints = face_ints[tets[tet_i].faces[i]];
		for (unsigned int j = 0; j < ints.size(); j++)
			initial_degen_points.push_back(
				initial_degen_point{ints[j]->pnt, 0, ints[j]});
	}

	if (initial_degen_points.empty())
		return;

	// Compute the field's QR decomposition, which will be used to find the
	// cokernel and convert from the veronese manifold back to R^3.
	field_QR_type field_QR(tet.field);
	tensor3 cokernel =
		field_QR.householderQ() * tensor3::Unit(tensor_size - 1);
	Matrix3ft cokernel_quad = tensor3_to_quadratic_eq(cokernel);

	// Make it neutral or planar for convenience.
	if (cokernel_quad.determinant() > 0.0)
	{
		cokernel *= -1;
		cokernel_quad *= -1;
	}

	// TODO: Maybe prevent linearly dependent fields from causing problems and
	// get at least four degenerate points by forcing the minimum pivot
	// coordinate to be 0 using find_min_piv_points.

	if (field_QR.nonzeroPivots() == 4)
	{
		// 0 kernel.

		veronese_ellipse ellipse(cokernel_quad);

		for (auto& p : initial_degen_points)
		{
			// Find the locations on the ellipse corresponding to the initial
			// degenerate points.
			p.theta = ellipse.get_angle(p.pnt.nondegen_vec);

			unsigned int face;
			for (face = 0; face < 4; face++)
				if (tets[tet_i].faces[face] == p.pnt.index)
					break;
			assert(face < 4);

			// Check which direction we are going.
			Vector3ft nondegen_vec = p.pnt.nondegen_vec;
			Vector3ft tangent = ellipse.get_tangent(nondegen_vec);
			float_type derivative = field_QR.solve(matrix_trace_to_tensor3(
				nondegen_vec * tangent.transpose() +
				tangent * nondegen_vec.transpose()))[face];

			p.entering_tet = ((derivative < 0.0) == (p.pnt.loc.w() < 0.0));
		}

		const float_type angle_per_sample = 2 * M_PI / 1000;

		for (auto& p : initial_degen_points)
		{
			if (!p.entering_tet)
				continue;

			initial_degen_point* closest_exit = NULL;
			float_type closest_dist = 10; // Ok, since > 2*pi.
			for (auto& q : initial_degen_points)
			{
				if (q.entering_tet || q.curve->visited)
					continue;

				// Don't connect to points the wrong side of infinity.
				if ((p.pnt.loc.w() < 0.0) ^ (q.pnt.loc.w() < 0.0))
					continue;

				float_type dist = q.theta - p.theta;
				if (dist < 0.0)
				{
					if (dist > -1e-14) // Silly hack for MSVC low precision
						dist = 0.0;
					else
						dist += 2 * M_PI;
				}
				if (dist < closest_dist)
				{
					closest_exit = &q;
					closest_dist = dist;
				}
			}
			assert(closest_exit);
			closest_exit->curve->visited = true;

			// Trace the degenerate curve from p to closest_exit. Sample evenly
			// with a distance close to angle_per_sample.

			float_type angle_diff = closest_dist;
			size_t num_samples = (size_t) (angle_diff / angle_per_sample);
			float_type segment_angle_per_sample = angle_diff / num_samples;

			float_type t = p.theta;
			degen_curve_head* last_head = p.curve;
			for (size_t i = 0; i < num_samples;
			     i++, t += segment_angle_per_sample)
			{
				Vector3ft nondegen_vec = ellipse.sample(t);
				Vector4ft bary;

				// Compute v*v^T to get an unscaled matrix, then find where that
				// occurs in the field.
				bary = field_QR.solve(matrix_trace_to_tensor3(
					nondegen_vec * nondegen_vec.transpose()));

				// Linear if loc_bary.sum() > 0. Planar if < 0.

				degen_point pnt{
					Matrix4ft(tet.loc.colwise().homogeneous()) * bary,
					nondegen_vec, tet_i, false};
				degen_curve_head* new_node = new degen_curve_head();
				new_node->pnt = pnt;
				new_node->link[0] = NULL;
				new_node->link[1] = NULL;
				new_node->visited = false;

				attach_lists(last_head, new_node);
				last_head = new_node;
			}

			// Attach endpoint's segment.
			attach_lists(last_head, closest_exit->curve);
		}
	}
	else
	{
		// >= 1D kernel. Degenerate planes (2D kernel) probably will fail.

		// Find the kernel of the field.
		Vector4ft kernel = Vector4ft::Unit(3);
		Matrix4ft R = field_QR.matrixR().topLeftCorner<4, 4>();
		R(3, 3) = 1.0;
		R.triangularView<Eigen::Upper>().solveInPlace(kernel);
		kernel.applyOnTheLeft(field_QR.colsPermutation());

		// Check if the kernel is inside the tet.
		if (kernel.sum() < 0.0)
			kernel = -kernel;
		bool inside = (kernel.array() >= 0.0).all();

		// Connect points with the same non-repeating eigenvector.
		for (auto& p : initial_degen_points)
		{
			if (p.curve->visited)
				// Already attached.
				continue;
			p.curve->visited = true;

			// Find the closest degenerate point (by non-repeating eigenvector).
			initial_degen_point* closest = NULL;
			// No squared distance can be bigger than 4, since the non-repeating
			// eigenvectors are normalized.
			float_type closest_sq_dist = 10;
			for (auto& q : initial_degen_points)
			{
				if (q.curve->visited)
					continue;

				Vector3ft q_evec = q.pnt.nondegen_vec;
				if (q_evec.dot(p.pnt.nondegen_vec) < 0.0)
					q_evec = -q_evec;
				float_type dist = (q_evec - p.pnt.nondegen_vec).squaredNorm();
				if (dist < closest_sq_dist)
				{
					closest = &q;
					closest_sq_dist = dist;
				}
			}
			assert(closest);

			degen_curve_head* head = p.curve;
			if (inside)
			{
				// TODO: Connect through triple degenerate point.

				Vector4ft kernel_p;
				if (p.pnt.loc.w() >= 0.0)
					kernel_p = kernel;
				else
					kernel_p = -kernel;

				degen_point triple_degen_p{
					Matrix4ft(tet.loc.colwise().homogeneous()) * kernel_p,
					p.pnt.nondegen_vec, tet_i, false};
				degen_curve_head* triple_degen_p_node = new degen_curve_head();
				triple_degen_p_node->pnt = triple_degen_p;
				triple_degen_p_node->link[0] = NULL;
				triple_degen_p_node->link[1] = NULL;
				triple_degen_p_node->visited = false;

				attach_lists(head, triple_degen_p_node);

				// Don't attach the two peices, as otherwise they would go
				// through infinity instead of the triple degenerate point.

				// Other side should match with closest.
				degen_point triple_degen_closest{
					-triple_degen_p.loc,
					p.pnt.nondegen_vec, tet_i, false};
				degen_curve_head* triple_degen_closest_node =
					new degen_curve_head();
				triple_degen_closest_node->pnt = triple_degen_closest;
				triple_degen_closest_node->link[0] = NULL;
				triple_degen_closest_node->link[1] = NULL;
				triple_degen_closest_node->visited = false;

				head = triple_degen_closest_node;
			}

			attach_lists(head, closest->curve);
			closest->curve->visited = true;
		}
	}

	// Clear the visited flags, as they will be used again later.
	for (auto& p : initial_degen_points)
		p.curve->visited = false;
}

void degenerate_curve_tracer::attach_lists(degen_curve_head* h0,
                                           degen_curve_head* h1)
{
	bool h0_empty_index = (h0->link[0] != NULL);
	bool h1_empty_index = (h1->link[0] != NULL);

	assert(h0->link[h0_empty_index] == NULL);
	assert(h1->link[h1_empty_index] == NULL);

	h0->link[h0_empty_index] = h1;
	h1->link[h1_empty_index] = h0;
}

void degenerate_curve_tracer::advance_head(degen_curve_head*& h,
                                           degen_curve_head*& p)
{
	degen_curve_head* old_head = h;
	h = h->link[h->link[0] == p];
	p = old_head;
	assert(!h || h->link[0] == p || h->link[1] == p);
}

void find_infinite_degenerates(const affine_field<3>& field)
{
	std::cout << field << '\n';

	// Compute the field's QR decomposition, which will be used to find the
	// cokernel and convert from the veronese manifold back to R^3.
	field_QR_type field_QR(field);
	std::cout << "Field kernel: " << field_QR.dimensionOfKernel() << ", ";
	std::cout << "Minimum pivot: " << field_QR.matrixR()(3, 3) << '\n';

	std::cout << "Q:\n";
	std::cout << Eigen::Matrix<float_type, tensor_size, tensor_size>
		(field_QR.householderQ()) << '\n';
	std::cout << "R:\n";
	std::cout << affine_field<3>
		(field_QR.matrixR().triangularView<Eigen::Upper>()) << "\n\n\n";

	tensor3 cokernel =
		field_QR.householderQ() * tensor3::Unit(tensor_size - 1);
	Matrix3ft cokernel_quad = tensor3_to_quadratic_eq(cokernel);

	// Make it neutral or planar for convenience.
	if (cokernel_quad.determinant() > 0.0)
	{
		cokernel *= -1;
		cokernel_quad *= -1;
	}

	// Prevent linearly dependent fields from causing problems and get at least
	// four degenerate points by forcing the minimum pivot coordinate to be 0.

	std::cout << "\n\n\n";

	if (field_QR.nonzeroPivots() < 3)
	{
		std::cerr << "Fields with degenerate planes are not supported.\n";
		exit(EXIT_FAILURE);
	}
	else if (field_QR.nonzeroPivots() == 4)
		find_degenerates_kernel0(field, field_QR, cokernel_quad);
	else if (field_QR.nonzeroPivots() == 3)
		find_degenerates_kernel1(field, field_QR, cokernel_quad);

	// Connect the degenerate points together.
	for (size_t i = 0, size = known_degen_points.size(); i < size; i++)
	{
		size_t prev_index = i ? i - 1 : size - 1;
		known_degen_lines.emplace_back(prev_index, i);
	}
}

static void
find_infinity_points(const affine_field<3>& field,
                     std::vector<initial_degen_point>& output_points)
{
	field_plane_QR_type field_QR(field.rightCols<3>() -
	                             field.col(0).replicate<1, 3>());
	std::cout << "Field kernel: " << field_QR.dimensionOfKernel() << ", ";
	std::cout << "Minimum pivot: " << field_QR.matrixR()(2, 2) << '\n';

	std::cout << "Q:\n";
	std::cout << Eigen::Matrix<float_type, tensor_size, tensor_size>
		(field_QR.householderQ()) << '\n';
	std::cout << "R:\n";
	std::cout << affine_field<2>
		(field_QR.matrixR().triangularView<Eigen::Upper>()) << "\n\n\n";

	tensor3 cokernel0 =
		field_QR.householderQ() * tensor3::Unit(tensor_size - 2);
	tensor3 cokernel1 =
		field_QR.householderQ() * tensor3::Unit(tensor_size - 1);
	Matrix3ft cokernel0_quad = tensor3_to_quadratic_eq(cokernel0);
	Matrix3ft cokernel1_quad = tensor3_to_quadratic_eq(cokernel1);

	int num_solutions_inf;
	Vector3ft_na solutions_inf[4];
	solve_homogeneous_quadratic(cokernel0_quad, cokernel1_quad,
	                            solutions_inf, num_solutions_inf);

	for (int i = 0; i < num_solutions_inf; i++)
	{
		Vector3ft nondegen_vec = solutions_inf[i];

		float_type len_sq = nondegen_vec.squaredNorm();
		if (len_sq == 0.0) // Don't bother with degenerate curves at infinity.
			continue;
		float_type error1 =
			nondegen_vec.transpose() * cokernel0_quad * nondegen_vec;
		float_type error2 =
			nondegen_vec.transpose() * cokernel1_quad * nondegen_vec;
		error1 /= len_sq;
		error2 /= len_sq;

		std::cout << "Error: " << error1 << ", " << error2 << '\n';

		Vector4ft loc;
		loc.tail<3>() = field_QR.solve
			(matrix_trace_to_tensor3(nondegen_vec * nondegen_vec.transpose()));
		loc[0] = -loc.tail<3>().sum();

		std::cout << nondegen_vec.transpose() <<
			" : " << barycentric_normalized(loc).transpose() <<
			" : " << loc.transpose() << '\n';

		degen_point pnt;
		pnt.loc = loc;
		pnt.nondegen_vec = nondegen_vec;
		output_points.emplace_back(initial_degen_point{pnt, 0.0});
	}
}

static void
find_min_piv_points(const affine_field<3>& field, const field_QR_type& field_QR,
                    const Matrix3ft& cokernel,
                    std::vector<initial_degen_point>& output_points)
{
	tensor3 min_pivot_cokernel =
		field_QR.householderQ() * tensor3::Unit(tensor_size - 2);
	Matrix3ft min_pivot_cokernel_quad =
		tensor3_to_quadratic_eq(min_pivot_cokernel);

	int num_solutions_min_piv;
	Vector3ft_na solutions_min_piv[4];
	solve_homogeneous_quadratic(cokernel, min_pivot_cokernel_quad,
	                            solutions_min_piv, num_solutions_min_piv);

	for (int i = 0; i < num_solutions_min_piv; i++)
	{
		Vector3ft nondegen_vec = solutions_min_piv[i];

		float_type len_sq = nondegen_vec.squaredNorm();
		float_type error1 =
			nondegen_vec.transpose() * cokernel * nondegen_vec;
		float_type error2 =
			nondegen_vec.transpose() * min_pivot_cokernel_quad * nondegen_vec;
		error1 /= len_sq;
		error2 /= len_sq;

		std::cout << "Error: " << error1 << ", " << error2 << '\n';

		// Solve the least squares. Don't use the last two rows to ensure
		// that numerical errors don't get amplified by solving R.
		Vector4ft loc = Vector4ft::Zero();
		loc.head<3>() =
			(field_QR.householderQ().setLength(3).inverse() *
			 matrix_trace_to_tensor3(nondegen_vec * nondegen_vec.transpose()))
			.head<3>();
		field_QR.matrixR().topLeftCorner<3, 3>()
			.triangularView<Eigen::Upper>()
			.solveInPlace(loc.head<3>());
		loc.applyOnTheLeft(field_QR.colsPermutation());

		std::cout << nondegen_vec.transpose() <<
			" : " << barycentric_normalized(loc).transpose() <<
			" : " << loc.transpose() << '\n';

		degen_point pnt;
		pnt.loc = loc;
		pnt.nondegen_vec = nondegen_vec;
		output_points.emplace_back(initial_degen_point{pnt, 0.0});
	}
}

static void find_degenerates_kernel0(const affine_field<3>& field,
                                     const field_QR_type& field_QR,
                                     const Matrix3ft& cokernel)
{
	// Initial tetrahedron, infinity, or plane intersections used to tell where
	// to trace.
	std::vector<initial_degen_point> initial_degen_points;

	find_min_piv_points(field, field_QR, cokernel, initial_degen_points);
	find_infinity_points(field, initial_degen_points);

	veronese_ellipse ellipse(cokernel);

	std::cout << "Cokernel eigenvalues: " <<
		ellipse.cokernel_eigen.eigenvalues().transpose() << '\n';

	std::cout << "semi_major_axis_inv_sq: " << ellipse.semi_major_axis_inv_sq <<
		", semi_minor_axis_inv_sq: " << ellipse.semi_minor_axis_inv_sq << '\n';

	// Find the locations on the ellipse corresponding to the initial degenerate
	// points.
	for (auto& p : initial_degen_points)
		p.theta = ellipse.get_angle(p.pnt.nondegen_vec);

	std::sort(initial_degen_points.begin(), initial_degen_points.end(),
	          initial_degen_point_ellipse_order());

	const float_type angle_per_sample = 2 * M_PI / 1000;

	const initial_degen_point* prev = &initial_degen_points.back();
	for (auto& p : initial_degen_points)
	{
		// Trace the degenerate curve from prev to it. Sample evenly with a
		// distance close to angle_per_sample.

		float_type angle_diff = p.theta - prev->theta;
		if (angle_diff < 0.0)
			angle_diff += 2 * M_PI;
		size_t num_samples = (size_t) (angle_diff / angle_per_sample);
		float_type segment_angle_per_sample = angle_diff / num_samples;

		float_type t = prev->theta;
		for (size_t i = 0; i < num_samples; i++, t += segment_angle_per_sample)
		{
			Vector3ft nondegen_vec = ellipse.sample(t);

			float_type error =
				nondegen_vec.transpose() * cokernel * nondegen_vec;

			Vector4ft loc_bary;

			// Compute v*v^T to get an unscaled matrix, then find where that
			// occurs in the field.
			loc_bary = field_QR.solve(matrix_trace_to_tensor3(
				nondegen_vec * nondegen_vec.transpose()));

			std::cout << nondegen_vec.transpose() << " : " << error <<
				" : " << barycentric_normalized(loc_bary).transpose() <<
				" : " << loc_bary.transpose() << '\n';

			// Linear if loc_bary.sum() > 0. Planar if < 0.

			degen_point pnt;
			pnt.loc = loc_bary;
			known_degen_points.emplace_back(pnt);
		}

		degen_point pnt;
		pnt.loc = p.pnt.loc;
		known_degen_points.emplace_back(pnt);

		prev = &p;
	}
}

static void find_degenerates_kernel1(const affine_field<3>& field,
                                     const field_QR_type& field_QR,
                                     const Matrix3ft& cokernel)
{
	// Initial tetrahedron, infinity, or plane intersections used to tell where
	// to trace.
	std::vector<initial_degen_point> initial_degen_points;

	find_min_piv_points(field, field_QR, cokernel, initial_degen_points);

	// Find the kernel of the field.
	Vector4ft kernel = Vector4ft::Unit(3);
	Matrix4ft R = field_QR.matrixR().topLeftCorner<4, 4>();
	R(3, 3) = 1.0;
	R.triangularView<Eigen::Upper>().solveInPlace(kernel);
	kernel.applyOnTheLeft(field_QR.colsPermutation());

	// initial_degen_points should contain 0, 2, or 4 degenerate points.
	// Alternate them with the kernel (with alternating signs), so that they
	// will be connected both ways.
	for (size_t i = 0, mid = initial_degen_points.size() / 2; i < mid; i++)
	{
		const initial_degen_point& p1 = initial_degen_points[i];
		const initial_degen_point& p2 = initial_degen_points[i + mid];

		degen_point pnt;
		pnt.loc = kernel;
		known_degen_points.emplace_back(pnt);

		pnt.loc = p1.pnt.loc;
		known_degen_points.emplace_back(pnt);

		pnt.loc = -kernel;
		known_degen_points.emplace_back(pnt);

		pnt.loc = p2.pnt.loc;
		known_degen_points.emplace_back(pnt);
	}

	for (const auto& p : known_degen_points)
	{
		std::cout << barycentric_normalized(p.loc).transpose() << " : " <<
			p.loc.transpose() << '\n';
	}
}

veronese_ellipse::veronese_ellipse(const Matrix3ft& cokernel_) :
	cokernel(cokernel_),
	cokernel_eigen(cokernel_)
{
	if (cokernel_eigen.info() != Eigen::Success)
	{
		std::cerr << "Error computing eigendecomposition.\n";
		exit(EXIT_FAILURE);
	}

	semi_major_axis_inv_sq =
		-cokernel_eigen.eigenvalues()[1] / cokernel_eigen.eigenvalues()[0];
	semi_minor_axis_inv_sq =
		-cokernel_eigen.eigenvalues()[2] / cokernel_eigen.eigenvalues()[0];
}

Vector3ft veronese_ellipse::sample(float_type angle)
{
	return cokernel_eigen.eigenvectors() * sample_eigenspace(angle);
}

Vector3ft veronese_ellipse::sample_eigenspace(float_type angle)
{
	// Sample the ellipse using the polar equation.
	Vector2ft cos_sin(cos(angle), sin(angle));
	float_type cos_sin_sq =
		semi_major_axis_inv_sq * cos_sin[0] * cos_sin[0] +
		semi_minor_axis_inv_sq * cos_sin[1] * cos_sin[1];
	float_type inv_cos_sin_sq_1 = 1.0 / (cos_sin_sq + 1.0);
	float_type r = sqrt(inv_cos_sin_sq_1);
	float_type x = r * sqrt(cos_sin_sq);

	return Vector3ft(x, r * cos_sin[0], r * cos_sin[1]);
}

float_type veronese_ellipse::get_angle(const Vector3ft& nondegen)
{
	return get_angle_eigenspace(cokernel_eigen.eigenvectors().transpose() *
	                            nondegen);
}

float_type veronese_ellipse::get_angle_eigenspace(Vector3ft nondegen)
{
	if (nondegen[0] < 0.0)
		nondegen = -nondegen;

	return atan2(nondegen[2], nondegen[1]);
}

Vector3ft veronese_ellipse::get_tangent(const Vector3ft& nondegen)
{
	Vector3ft eig_space = cokernel_eigen.eigenvectors().transpose() * nondegen;
	return cokernel_eigen.eigenvectors() * get_tangent_eigenspace(eig_space);
}

Vector3ft veronese_ellipse::get_tangent_eigenspace(const Vector3ft& nondegen)
{
	Vector3ft tangent =
		nondegen.cross(cokernel_eigen.eigenvalues().asDiagonal() * nondegen);
	if (nondegen[0] < 0)
		tangent = -tangent;
	float_type len = tangent.norm();
	if (len == 0.0) // Can only occur at medium eigenvector.
		tangent = Vector3ft::UnitZ() * nondegen[1];
	else
		tangent /= len;
	return tangent;
}

// Compute the intersection of two homogeneous quadratic equations. The result
// is up to four vectors, each on its own line of solutions. Assumes that both
// quadratics are traceless. Roots will be added according to their
// multiplicity.
static void solve_homogeneous_quadratic(Matrix3ft quad1, Matrix3ft quad2,
                                        Vector3ft_na solutions[4],
                                        int& num_solutions)
{
	num_solutions = 0;

	// Step 1: Find a degenerate linear combination of quad1 and quad2. Call it
	//         quad0 = lambda * quad1 + mu * quad2.

	Eigen::RealQZ<Matrix3ft> qz(quad1, quad2, true);

	if (qz.info() != Eigen::Success)
	{
		std::cerr << "Could not preform QZ decomposition.\n";
		exit(EXIT_FAILURE);
	}

	// quad1 == qz.matrixQ() * qz.matrixS() * qx.matrixZ()
	// quad2 == qz.matrixQ() * qz.matrixT() * qx.matrixZ()
	// qz.matrixT() is upper triangular, but S is only block upper
	// triangular. qz.matrixS()(2, 0) should always be 0.
	// qz.matrixS()(1, 0) or qz.matrixS(2, 1) should be zero.

	// Find a real root.
	bool upperLeftRoot = (qz.matrixS()(1, 0) == 0.0);
	int col = upperLeftRoot ? 0 : 2;
	Vector2ft lambda_mu(qz.matrixT()(col, col), -qz.matrixS()(col, col));

	float_type lambda_mu_norm = lambda_mu.norm();
	if (lambda_mu_norm > 0.0)
		lambda_mu /= lambda_mu_norm;
	else
		// If both matrices are singular then just use the second one.
		lambda_mu << 0.0, 1.0;

	// Step 2: Find the kernel of quad0, and find a orthonormal basis for
	// vectors orthogonal to the kernel. Use Z^T if upperLeftRoot, and Q
	// otherwise. Column col in the basis is the kernel of quad0. Use these to
	// put either quad1 or quad2 (whichever is most different from quad0) in the
	// new basis.
	//
	// Also compute the quadratic in the new basis. Only use 2x2 matrix as
	// both row and column col would be 0. The new quadratic will be symmetric
	// and traceless, so only store the top row/left column.
	Matrix3ft to_global;
	if (upperLeftRoot)
		to_global = qz.matrixZ().transpose();
	else
		to_global = qz.matrixQ();

	Matrix3ft rotated_quadratic;
	if (fabs(lambda_mu[0]) <= fabs(lambda_mu[1]))
		rotated_quadratic = to_global.transpose() * quad1 * to_global;
	else
		rotated_quadratic = to_global.transpose() * quad2 * to_global;

	Vector2ft quad_2d;
	if (upperLeftRoot)
		quad_2d = qz.matrixZ().bottomRows<2>() * qz.matrixQ() *
			(lambda_mu[0] * qz.matrixS().col(1) +
			 lambda_mu[1] * qz.matrixT().col(1));
	else
		quad_2d = ((lambda_mu[0] * qz.matrixS().row(0) +
		            lambda_mu[1] * qz.matrixT().row(0)) *
		           qz.matrixZ() * qz.matrixQ().leftCols<2>()).transpose();

	// Step 3: Solve the degenerate quadratic.

	// If quad_2d = (a, b), then the corresponding matrix will be [a  b]
	//                                                            [b -a]
	// The quadratic has two solutions given by
	//  a*x + (b + sgn(b)*sqrt(b^2 + a^2))*y = 0
	// -(b + sgn(b)*sqrt(b^2 + a^2))*x + a*y = 0
	float_type sqrt_part = copysign(quad_2d.norm(), quad_2d[1]);
	Vector2ft line0_2d(quad_2d[0], quad_2d[1] + sqrt_part);
	Vector2ft line1_2d(-line0_2d[1], line0_2d[0]);

	// Step 4: Find the intersections of the lines with one of the original
	// quadratics.

	solve_homogeneous_quadratic_helper_line_intersection
		(rotated_quadratic, line0_2d, upperLeftRoot, col, to_global,
		 solutions, num_solutions);

	solve_homogeneous_quadratic_helper_line_intersection
		(rotated_quadratic, line1_2d, upperLeftRoot, col, to_global,
		 solutions + num_solutions, num_solutions);
}

static void solve_homogeneous_quadratic_helper_line_intersection
	(const Matrix3ft& quad, const Vector2ft& line, bool upperLeftRoot, int col,
	 const Matrix3ft& to_global, Vector3ft_na solutions[2], int& num_solutions)
{
	// Require the solution to by on the plane give by line and
	// to_global.col(col).
	// Equivalent to multiplying on both sides by [1 0   ]
	//                                            [0 line]
	// Symmetric matrix [a b]
	//                  [b c]
	float_type a, b, c;
	a = quad(col, col);
	if (upperLeftRoot)
	{
		b = quad.row(0).tail<2>() * line;
		c = line.transpose() * quad.bottomRightCorner<2, 2>() * line;
	}
	else
	{
		b = quad.row(2).head<2>() * line;
		c = line.transpose() * quad.topLeftCorner<2, 2>() * line;
	}

	Eigen::Matrix<float_type, 3, 2> to_global_from_line;
	to_global_from_line.col(0) = to_global.col(col);
	if (upperLeftRoot)
		to_global_from_line.col(1) = to_global.rightCols<2>() * line;
	else
		to_global_from_line.col(1) = to_global.leftCols<2>() * line;

	auto roots = quadratic::real_roots(a, b, c);
	solutions[0] = to_global_from_line * roots.second.col(0);
	solutions[1] = to_global_from_line * roots.second.col(1);
	num_solutions += roots.first;
}

void create_tensor_field(const char* file, affine_field<3>& out)
{
	std::fstream fs(file, std::ios::in);

	std::string line;
	int i = 0;
	while (std::getline(fs, line) && i < 4)
	{
		if (line.empty())
			continue;

		Eigen::Matrix<float_type, 6, 1> t;
		if (!(std::stringstream(line) >> t[0] >> t[1] >> t[2] >> t[3] >> t[4] >>
		      t[5]))
		{
			std::cerr << "Invalid tensor data\n";
			exit(1);
		}

		// Remove the isotropic component as the trace does not effect the
		// degenerate lines.
		float_type trace_3 = (t[0] + t[3] + t[5]) / 3;
		t[0] -= trace_3;
		t[3] -= trace_3;

		out.col(i) = t.head<tensor_size>();

		// Old bug. Don't want to break all the fields now, so calling it a
		// feature.
		if (i == 3)
			out.leftCols<3>() += out.col(i).replicate<1, 3>();

		i++;
	}

	if (i < 4)
	{
		std::cerr << "Not enough tensors\n";
		exit(1);
	}
}

void create_tensor_field_lintens3d(const char* file, affine_field<3>& field)
{
	std::fstream fs(file, std::ios::in);

	std::string line;
	int i = 0;
	while (i < 4 && std::getline(fs, line))
	{
		if (line.empty())
			continue;

		tensor3 t;
		if (!(std::stringstream(line) >> t[0] >> t[1] >> t[2] >> t[3] >> t[4]))
		{
			std::cerr << "Invalid tensor data\n";
			exit(1);
		}

		// File order: 0, x, y, z
		field.col(i ? i - 1 : 3) = t;
		i++;
	}

	if (i < 4)
	{
		std::cerr << "Not enough tensors\n";
		exit(EXIT_FAILURE);
	}
}

void create_tensor_field_tetfield(const char* file,
                                  detail::detail_tetrahedron& tet)
{
	std::fstream fs(file, std::ios::in);

	std::string line;
	std::getline(fs, line);
	if (line != "tet")
	{
		std::cerr << "Not a tet field\n";
		exit(EXIT_FAILURE);
	}

	int i = 0;
	while (i < 4 && std::getline(fs, line))
	{
		if (line.empty())
			continue;

		Vector3ft p;
		if (!(std::stringstream(line) >> p[0] >> p[1] >> p[2]))
		{
			std::cerr << "Invalid location data\n";
			exit(1);
		}

		tet.loc.col(i) = p;
		i++;
	}

	if (i < 4)
	{
		std::cerr << "Not enough vertices\n";
		exit(EXIT_FAILURE);
	}

	i = 0;
	while (i < 4 && std::getline(fs, line))
	{
		if (line.empty())
			continue;

		tensor3 t;
		if (!(std::stringstream(line) >> t[0] >> t[1] >> t[2] >> t[3] >> t[4]))
		{
			std::cerr << "Invalid tensor data\n";
			exit(1);
		}

		tet.field.col(i) = t;
		i++;
	}

	if (i < 4)
	{
		std::cerr << "Not enough tensors\n";
		exit(EXIT_FAILURE);
	}
}
