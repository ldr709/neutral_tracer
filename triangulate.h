#ifndef TENSOR_LINES_TRIANGULATE_H
#define TENSOR_LINES_TRIANGULATE_H

#include <functional>
#include <vector>
#include "tensor.h"

namespace triangulate
{

typedef std::array<size_t, 3> triangle;

std::vector<triangle> triangulate_bounded_region(
	const Vector2ft* vertices, size_t num_vertices,
	const size_t* boundary_ends, size_t num_boundaries,
	std::function<std::vector<Vector2ft_na>(const triangle*, size_t)> subdivide);

}

#endif
