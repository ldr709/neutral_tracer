#include "triangulate.h"

// Partly copied (with modifications) from
// https://doc.cgal.org/latest/Triangulation_2/Triangulation_2_2polygon_triangulation_8cpp-example.html

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <array>
#include <iostream>
#include <utility>
#include <vector>

namespace triangulate
{

namespace
{

struct vertex_info
{
	size_t index;
};

struct FaceInfo2
{
	FaceInfo2(){}
	int nesting_level;
	bool in_domain()
	{
		return nesting_level % 2 == 1;
	}
};

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<vertex_info, K> Vb;
typedef CGAL::Triangulation_face_base_with_info_2<FaceInfo2, K> Fbb;
typedef CGAL::Constrained_triangulation_face_base_2<K, Fbb> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb> TDS;
typedef CGAL::No_intersection_tag Itag;
typedef CGAL::Constrained_Delaunay_triangulation_2<K, TDS, Itag> CDT;
typedef CDT::Point Point;

void mark_domains(CDT& ct,
                  CDT::Face_handle start,
                  int index, std::list<CDT::Edge>& border)
{
	if (start->info().nesting_level != -1)
	{
		return;
	}
	std::list<CDT::Face_handle> queue;
	queue.push_back(start);
	while (!queue.empty())
	{
		CDT::Face_handle fh = queue.front();
		queue.pop_front();
		if (fh->info().nesting_level == -1)
		{
			fh->info().nesting_level = index;
			for (int i = 0; i < 3; i++)
			{
				CDT::Edge e(fh, i);
				CDT::Face_handle n = fh->neighbor(i);
				if (n->info().nesting_level == -1)
				{
					if (ct.is_constrained(e))
						border.push_back(e);
					else
						queue.push_back(n);
				}
			}
		}
	}
}

// Explore set of facets connected with non constrained edges, and attribute to
// each such set a nesting level. We start from facets incident to the infinite
// vertex, with a nesting level of 0. Then we recursively consider the
// non-explored facets incident to constrained edges bounding the former set and
// increase the nesting level by 1. Facets in the domain are those with an odd
// nesting level.
void mark_domains(CDT& cdt)
{
	for (CDT::All_faces_iterator it = cdt.all_faces_begin(); it != cdt.all_faces_end(); ++it)
	{
		it->info().nesting_level = -1;
	}
	std::list<CDT::Edge> border;
	mark_domains(cdt, cdt.infinite_face(), 0, border);
	while (! border.empty())
	{
		CDT::Edge e = border.front();
		border.pop_front();
		CDT::Face_handle n = e.first->neighbor(e.second);
		if (n->info().nesting_level == -1)
			mark_domains(cdt, n, e.first->info().nesting_level+1, border);
	}
}

void insert_vertices(
	CDT& cdt, const Vector2ft* vertices, size_t num_vertices,
	const size_t* boundary_ends, size_t num_boundaries)
{
	size_t i;
	size_t current_boundary = 0;
	CDT::Vertex_handle v_first, v_prev;
	for (i = 0;; i++)
	{
		bool first_in_boundary = false;
		if (i == boundary_ends[current_boundary])
		{
			first_in_boundary = true;
			cdt.insert_constraint(v_prev, v_first);

			if (++current_boundary >= num_boundaries)
				break;
		}
		else if (i == 0)
			first_in_boundary = true;

		Vector2ft p = vertices[i];
		CDT::Vertex_handle v = cdt.insert(Point(p.x(), p.y()));
		v->info().index = i;

		if (first_in_boundary)
			v_first = v;
		else
			cdt.insert_constraint(v_prev, v);
		v_prev = v;
	}

	for (; i < num_vertices; i++)
	{
		Vector2ft p = vertices[i];
		CDT::Vertex_handle v = cdt.insert(Point(p.x(), p.y()));
		v->info().index = i;
	}
}

void extract_triangulation(CDT& cdt, std::vector<triangle>& triangles_out)
{
	for (CDT::Finite_faces_iterator fit = cdt.finite_faces_begin();
	     fit != cdt.finite_faces_end(); ++fit)
	{
		if (!fit->info().in_domain())
			continue;

		triangle tri;
		for (unsigned int i = 0; i < 3; i++)
			tri[i] = fit->vertex(i)->info().index;
		triangles_out.push_back(tri);
	}
}

}

// Triangulate the provided region. Returns triangulation as a list of
// triangles, which reference vertices by their indexes. boundary_ends should
// have num_boundaries elemnts. The range
// [i ? boundary_ends[i-1] : 0, boundary_ends[i]) gives the vertices on boundary
// i. [boundary_ends[num_boundaries-1], num_vertices) contains any extra
// vertices to add. No boundary should be empty. There must be at least one
// boundary.
//
// After this initial triangulation, subdivide is called with the list of
// triangles. If it returns a non-empty vector then those vertices are added
// to the triangulation.
std::vector<triangle> triangulate_bounded_region(
	const Vector2ft* vertices, size_t num_vertices,
	const size_t* boundary_ends, size_t num_boundaries,
	std::function<std::vector<Vector2ft_na>(const triangle*, size_t)> subdivide)
{
	CDT cdt;
	insert_vertices(cdt, vertices, num_vertices, boundary_ends, num_boundaries);

	// Mark facets that are inside the domain bounded by the polygon
	mark_domains(cdt);

	std::vector<triangle> triangles;
	extract_triangulation(cdt, triangles);

	auto new_verts = subdivide(&triangles[0], triangles.size());
	if (!new_verts.empty())
	{
		triangles.clear();

		for (size_t i = num_vertices, j = 0; j < new_verts.size(); i++, j++)
		{
			Vector2ft p = new_verts[j];
			CDT::Vertex_handle v = cdt.insert(Point(p.x(), p.y()));
			v->info().index = i;
		}

		mark_domains(cdt);
		extract_triangulation(cdt, triangles);
	}

	return triangles;
}

}
