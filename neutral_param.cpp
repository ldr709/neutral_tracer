#include "neutral_param.h"

#include <algorithm>
#include <set>
#include <utility>
#include "neutral.h"
#include "quaternion_group.h"
#include "triangulate.h"

neutral_tracer_param::neutral_tracer_param(
	const char* shader_prefix,
	const std::vector<vertex>& vertices,
	const std::vector<face>& faces,
	const std::vector<tet>& tets,
	size_t boundary_samples,
	size_t elliptic_x_cells,
	size_t elliptic_y_cells) :
	vertices(vertices),
	faces(faces),
	tets(tets),
	boundary_samples(boundary_samples),
	elliptic_x_cells(elliptic_x_cells),
	elliptic_y_cells(elliptic_y_cells)
{
	tet_infos.resize(tets.size());
	face_infos.resize(faces.size());
	size_t i = 0;
	for (const auto& t : tets)
	{
		tet_info& ti = tet_infos[i];

		auto loc = get_tet_loc(t);
		auto field = get_tet_field(t);

		ti.loc_inv =
			(loc.rightCols<3>() - loc.col(0).replicate<1, 3>()).inverse();

		ti.fieldQR.compute(field);
		tensor3 cokernel = ti.fieldQR.householderQ() * tensor3::Unit(4);
		Matrix3ft cokernel_quad = tensor3_to_quadratic_eq(cokernel);

		// Make it neutral or linear for convenience.
		if (cokernel_quad.determinant() < 0.0)
			cokernel_quad *= -1;

		// Find the singularities' medium eigenvectors.
		{
			Eigen::SelfAdjointEigenSolver<Matrix3ft> eigen(cokernel_quad);
			ti.eigenvectors = eigen.eigenvectors();
			ti.eigenvalues = eigen.eigenvalues();
		}

		// Use a right-handed system of eigenvectors.
		if (ti.eigenvectors.determinant() < 0.0)
			ti.eigenvectors.col(1) = -ti.eigenvectors.col(1);

		float_type cokernel_min = ti.eigenvalues[0];
		float_type cokernel_mid = ti.eigenvalues[1];
		float_type cokernel_max = ti.eigenvalues[2];

		float_type sqrt_min = sqrt(cokernel_mid - cokernel_min);
		float_type sqrt_max = sqrt(cokernel_max - cokernel_mid);
		Vector2ft normal0(sqrt_min, sqrt_max);
		normal0.normalize();
		ti.sings[0] =
			(ti.eigenvectors * Vector3ft(normal0[0], 0.0, normal0[1]));
		ti.sings[1] =
			(ti.eigenvectors * Vector3ft(-normal0[0], 0.0, normal0[1]));

		// Create adjacent edge infos.
		for (unsigned int j = 0; j < 4; j++)
			for (unsigned int k = j + 1; k < 4; k++)
				edge_infos[edge(t.verts[j], t.verts[k])] = edge_info();

		// Attach to adjacent faces.
		for (unsigned int j = 0; j < 4; j++)
		{
			auto& fi = face_infos[t.faces[j]];
			fi.tet[fi.num_tets++] = i;
		}

		i++;
	}

	// Find edge intersections.
	for (auto& kv : edge_infos)
	{
		const edge& e = kv.first;
		edge_info& ei = kv.second;

		Eigen::Matrix<float_type, 3, 2> edge_loc = get_edge_loc(e);

		Matrix3ft field[2];
		for (unsigned int v = 0; v < 2; v++)
			field[v] = tensor3_to_matrix(vertices[e[v]].field);
		Eigen::GeneralizedEigenSolver<Matrix3ft> solver(field[0], field[1]);

		// TODO: Maybe use determinant to find out how many solutions to use.
		for (unsigned int i = 0; i < 3; i++)
		{
			std::complex<float_type> alpha = solver.alphas()[i];
			float_type beta = solver.betas()[i];

			// Don't bother with complex roots.
			if (alpha.imag() != 0.0)
				continue;

			// det(beta*A + -alpha*B) = 0
			Vector2ft solution(beta, -alpha.real());
			if (solution.x() < 0.0)
				solution = -solution;
			if (solution.y() < 0.0)
				continue;
			solution = barycentric_normalized(solution);

			Vector3ft mid_evec = solver.eigenvectors().col(i).real();

			neutral_vert_tetedge v_te;
			v_te.neutral_v = neutral_verts.size();
			v_te.bary = solution;

			neutral_verts.emplace_back();
			neutral_vert& v = neutral_verts.back();
			v.loc = edge_loc * solution;
			v.mid = mid_evec;
			v.kind = TETEDGE;
			v.index[0] = e[0];
			v.index[1] = e[1];

			ei.intersections[ei.num_intersections++] = v_te;
		}
	}

	// Sample face intersections.
	i = 0;
	for (const auto& f : faces)
	{
		auto& fi = face_infos[i];
		assert(fi.num_tets > 0);

		compute_face_info(i, f, fi);

		i++;
	}

	for (i = 0; i < tets.size(); i++)
	{
		const auto& t = tets[i];
		auto& ti = tet_infos[i];
		find_tet_boundaries(i, t, ti);

		// If there are no boundaries, then there is no neutral surface.
		if (ti.boundary_ends.empty())
			continue;

		float_type cokernel_min = ti.eigenvalues[0];
		float_type cokernel_mid = ti.eigenvalues[1];
		float_type cokernel_max = ti.eigenvalues[2];

		float_type f_sq =
			(cokernel_mid - cokernel_min) / (cokernel_max - cokernel_mid);
		float_type f = std::sqrt(f_sq);

		// Correctly directed medium eigenvectors.
		std::vector<Vector3ft_na> tet_mids;

		// ... in stereographic coordinates.
		std::vector<Vector2ft, Eigen::aligned_allocator<Vector2ft>> tet_stereos;

		// Rotation so that z-axis is somewhere odd for stereographic
		// projection. Was randomly chosen, so if the fields don't get adjusted
		// based on this the z-axis shouldn't be near anything special.
		Matrix3ft rot;
		rot <<
			 0.4445186783485625, -0.0000000000000000, -0.8957695823141389,
			-0.3398884343338216,  0.9252174335151421, -0.1686669882512796,
			 0.8287816339696185,  0.3794373475551055,  0.4112764307311998;

		// Check if z-axis is on the correct side of infinity (i.e., negative
		// w). If it is inside then rotate 180-degrees so that it is outside (if
		// v is inside, then -v is automatically outside).
		if (!tet_mid_has_wrong_sign(t, rot.col(2)))
		{
			rot.col(0) = -rot.col(0);
			rot.col(2) = -rot.col(2);
		}

		tet_mids.reserve(ti.tet_verts.size());
		tet_stereos.reserve(ti.tet_verts.size());

		Vector3ft prev_mid;
		std::set<size_t> neutral_verts_seen;
		for (size_t i = 0; i < ti.boundary_ends.size(); i++)
		{
			size_t start = i ? ti.boundary_ends[i - 1] : 0;
			size_t end = ti.boundary_ends[i];

			const auto first_tetv = ti.tet_verts[start];
			Vector3ft prev;
			if (first_tetv.neutral_v[1] == (size_t) -1)
				prev = neutral_verts[first_tetv.neutral_v[0]].mid;
			else
				prev = neutral_verts[ti.tet_verts[start + 1].neutral_v[0]].mid;

			if (tet_mid_has_wrong_sign(t, prev))
				prev = -prev;

			for (size_t j = start; j < end; j++)
			{
				const auto& v = ti.tet_verts[j];
				Vector3ft mid = neutral_verts[v.neutral_v[0]].mid;
				if (mid.dot(prev) < 0.0)
					mid = -mid;
				assert(v.neutral_v[1] != (size_t) -1 ||
				       !tet_mid_has_wrong_sign(t, mid));
				prev = mid;

				tet_mids.push_back(mid);
				tet_stereos.push_back(to_stereographic(rot.transpose() * mid));

				// Add the normal (for averaging).
				for (unsigned int k = 0; k < 2; k++)
				{
					size_t nv = v.neutral_v[k];
					if (nv == (size_t) -1)
						continue;

					// TODO: Try unnormalized normals.
					if (neutral_verts_seen.insert(nv).second)
						neutral_verts[nv].normal +=
							mid_evec_to_normal(t, ti, mid).normalized();
				}
			}
		}
		neutral_verts_seen.clear();

		auto triangles = triangulate::triangulate_bounded_region(
			&tet_stereos[0], ti.tet_verts.size(),
			&ti.boundary_ends[0], ti.boundary_ends.size(),
			[&](const triangulate::triangle* tris, size_t count)
			{
				std::vector<Vector2ft_na> extra_pnts;
				for (size_t j = 0; j < count; j++)
					add_interior_verts(i, t, ti, f, f_sq, rot, tet_mids,
					                   extra_pnts, tris[j]);
				return extra_pnts;
			});

		for (const auto& tri : triangles)
		{
			neutral_triangle out_tri;
			for (unsigned int j = 0; j < 3; j++)
				out_tri.neutral_v[j] = ti.tet_verts[tri[j]].neutral_v[0];
			neutral_triangles.push_back(out_tri);
		}

		// Singularities have two vertices, so they each need an extra triangle.
		for (size_t j = 0; j < ti.boundary_ends.back(); j++)
		{
			const auto& sing_tetv = ti.tet_verts[j];
			if (sing_tetv.neutral_v[1] == (size_t) -1)
				continue;

			const auto& other_tetv = ti.tet_verts[sing_tetv.next_v];
			// Cannot be two singularities in a row.
			assert(other_tetv.neutral_v[1] == (size_t) -1);

			// TODO: Winding order.
			neutral_triangle out_tri =
			{
				sing_tetv.neutral_v[0],
				sing_tetv.neutral_v[1],
				other_tetv.neutral_v[0]
			};
			neutral_triangles.push_back(out_tri);
		}
	}

	// Renormalise normals (for averaging).
	for (auto& v : neutral_verts)
		v.normal.normalize();

	init_gl(shader_prefix);
}

neutral_tracer_param::~neutral_tracer_param()
{
	glDeleteVertexArrays(1, &neutral_vao);
	glDeleteBuffers(1, &lighting_buffer);
	glDeleteBuffers(1, &neutral_surface_buffer);
	glDeleteBuffers(1, &neutral_index_buffer);
	glDeleteProgram(neutral_shader_program);
}

void neutral_tracer_param::compute_face_info(
	size_t face_index, const face& f, face_info& fi)
{
	Matrix3ft face_loc = get_face_loc(f);
	Eigen::Matrix<float_type, 5, 3> face_field = get_face_field(f);
	auto cokernel = get_kernel<2>(face_field.transpose());

	// TODO: Maybe orthogonalize cokernel, as it was orthogonal in the wrong
	// metric.

	typedef face_param_starting_vert starting_vert;
	std::vector<starting_vert, Eigen::aligned_allocator<starting_vert>>
		starting_verts;

	// Add tetedge intersections.
	for (unsigned int j = 0; j < 3; j++)
	{
		unsigned int v0 = (j + 1) % 3;
		unsigned int v1 = (j + 2) % 3;
		if (f.verts[v0] > f.verts[v1])
			std::swap(v0, v1);

		edge_info& ei = edge_infos.at(edge(f.verts[v0], f.verts[v1]));

		for (unsigned int k = 0; k < ei.num_intersections; k++)
		{
			neutral_vert_tetedge& v_te = ei.intersections[k];
			neutral_vert_tetface v_tf;
			v_tf.neutral_v = v_te.neutral_v;

			v_tf.bary.setZero();
			v_tf.bary[v0] = v_te.bary[0];
			v_tf.bary[v1] = v_te.bary[1];

			Vector3ft mid = neutral_verts[v_te.neutral_v].mid;
			Eigen::Matrix<float_type, 5, 5> constraints;
			constraints.leftCols<2>() = cokernel;
			constraints.col(2) <<  mid[0], mid[1], mid[2],       0,      0;
			constraints.col(3) <<       0, mid[0],      0,  mid[1], mid[2];
			constraints.col(4) << -mid[2],      0, mid[0], -mid[2], mid[1];

			Vector5ft solution = get_kernel<1>(constraints);
			Vector2ft param = solution.head<2>();
			if (param.y() < 0.0)
				param = -param;
			float_type theta = atan2(param.y(), param.x());

			starting_vert start_v;
			start_v.v[0] = v_tf;
			start_v.present[0] = true;
			start_v.theta = theta;

			Vector3ft other_mid = solution.tail<3>().normalized();
			Vector3ft other_bary = face_mid_to_bary(f, other_mid);
			if ((other_bary.array() >= 0.0).all())
			{
				size_t other_v_index = neutral_verts.size();
				neutral_verts.emplace_back();
				neutral_vert& other_v = neutral_verts.back();

				other_v.loc = face_loc * other_bary;
				other_v.mid = other_mid;
				other_v.kind = TETFACE;
				other_v.index[0] = face_index;

				start_v.v[1].neutral_v = other_v_index;
				start_v.v[1].bary = other_bary;
				start_v.present[1] = true;
			}
			else
				start_v.present[1] = false;

			starting_verts.emplace_back(start_v);
		}
	}

	// Add adjacent singularities.
	for (unsigned int j = 0; j < fi.num_tets; j++)
	{
		tet_info& ti = tet_infos[fi.tet[j]];
		tensor3 tet_cokernel = ti.fieldQR.householderQ() * tensor3::Unit(4);

		starting_vert start_v;
		start_v.present[0] = false;
		start_v.present[1] = false;
		for (unsigned int sing = 0; sing < 2; sing++)
		{
			Vector3ft mid = ti.sings[sing];
			Vector3ft bary = face_mid_to_bary(f, mid);

			if ((bary.array() < 0.0).any())
				continue;

			size_t v_sing_index = neutral_verts.size();
			neutral_verts.emplace_back();
			neutral_vert& v_sing = neutral_verts.back();

			v_sing.loc = face_loc * bary;
			v_sing.mid = mid;
			v_sing.kind = SINGULARITY;
			v_sing.index[0] = face_index;

			start_v.v[sing].neutral_v = v_sing_index;
			start_v.v[sing].bary = bary;
			start_v.v[sing].flag = (j << 1) | sing;
			start_v.present[sing] = true;
		}

		if (!start_v.present[0] && !start_v.present[1])
			continue;

		Vector2ft param = cokernel.transpose() * tet_cokernel;
		if (param.y() < 0.0)
			param = -param;
		start_v.theta = atan2(param.y(), param.x());

		starting_verts.emplace_back(start_v);
	}

	// If nothing intersects the face, then we are done.
	if (starting_verts.empty())
		return;

	// Sort by parameter.
	std::sort(starting_verts.begin(), starting_verts.end(),
		[](const starting_vert& a, const starting_vert& b)
		{
			return a.theta < b.theta;
		});

	const float_type angle_per_sample = M_PI / boundary_samples;
	auto q = Eigen::Quaternion<float_type>::Identity();
	starting_vert* prev = &starting_verts.back();

	// Figure out if we are starting inside or outside the face.
	bool inside[2];
	{
		float_type theta = std::min(prev->theta + angle_per_sample,
		                            0.5 * (prev->theta - M_PI));
		auto mids = sample_face_param(cokernel, q, theta);
		for (unsigned int j = 0; j < 2; j++)
			inside[j] = (face_mid_to_bary(f, mids.col(j)).array() >= 0.0).all();
	}

	auto prev_mids = sample_face_param(cokernel, q, prev->theta - M_PI);
	reorder_face_sample(*prev, prev_mids);
	assert((prev->present[0] || !inside[0]) &&
	       (prev->present[1] || !inside[1]));

	// Trace out the intersection points.
	for (size_t j = 0; j < starting_verts.size(); j++)
	{
		auto& next = starting_verts[j];

		float_type prev_theta = prev->theta;
		if (j == 0)
			prev_theta -= M_PI;

		float_type angle_diff = next.theta - prev_theta;
		size_t num_segments = (size_t) (angle_diff / angle_per_sample);
		float_type angle_per_segment = angle_diff / num_segments;

		face_info::trace traces[2];
		for (unsigned int pnt = 0; pnt < 2; pnt++)
			if (inside[pnt])
				traces[pnt].push_back(prev->v[pnt]);

		float_type theta = prev_theta + angle_per_segment;
		for (size_t k = 1; k < num_segments;
		     k++, theta += angle_per_segment)
		{
			auto mids = sample_face_param(cokernel, q, theta);
			for (unsigned int pnt = 0; pnt < 2; pnt++)
			{
				if (!inside[pnt])
					continue;

				Vector3ft mid = mids.col(pnt);
				Vector3ft bary = face_mid_to_bary(f, mid);

				assert((bary.array() >= 0.0).all());

				size_t v_index = neutral_verts.size();
				neutral_verts.emplace_back();
				neutral_vert& v = neutral_verts.back();

				v.loc = face_loc * bary;
				v.mid = mid;
				v.kind = TETFACE;
				v.index[0] = face_index;

				neutral_vert_tetface v_tf;
				v_tf.neutral_v = v_index;
				v_tf.bary = bary;
				traces[pnt].emplace_back(v_tf);
			}
		}

		prev_mids = sample_face_param(cokernel, q, next.theta);
		reorder_face_sample(next, prev_mids);
		assert((next.present[0] || !inside[0]) &&
		       (next.present[1] || !inside[1]));

		for (unsigned int pnt = 0; pnt < 2; pnt++)
		{
			if (inside[pnt])
			{
				traces[pnt].push_back(next.v[pnt]);
				fi.intersections.emplace_back(std::move(traces[pnt]));
			}

			if (next.present[pnt] &&
			    neutral_verts[next.v[pnt].neutral_v].kind == TETEDGE)
				inside[pnt] = !inside[pnt];
		}

		prev = &next;
	}
}

void neutral_tracer_param::find_tet_boundaries(
	size_t tet_index, const tet& t, tet_info& ti)
{
	auto& tet_verts = ti.tet_verts;

	std::vector<bool> used_traces[4];
	for (unsigned int face = 0; face < 4; face++)
		used_traces[face].resize(
			face_infos[t.faces[face]].intersections.size(), false);

	for (unsigned int start_face = 0; start_face < 4; start_face++)
	{
		const face_info& start_face_info = face_infos[t.faces[start_face]];
		const auto& start_face_intersections =
			face_infos[t.faces[start_face]].intersections;
		for (size_t start_trace_i = 0;
		     start_trace_i < start_face_intersections.size(); start_trace_i++)
		{
			if (used_traces[start_face][start_trace_i])
				continue;

			const auto& start_trace = start_face_intersections[start_trace_i];
			const neutral_vert_tetface& start_vert = start_trace.front();

			// Trace the loop starting at start_trace.
			unsigned int face = start_face;
			size_t trace_i = start_trace_i;
			const auto* trace = &start_trace;
			const size_t first_v_index = tet_verts.size();
			size_t last_sing_neutral_v = -1;
			bool reversed = false;

			while (true)
			{
				const auto& cur_face_info = face_infos[t.faces[face]];
				used_traces[face][trace_i] = true;

				tet_verts.reserve(tet_verts.size() + trace->size() - 1);
				auto trace_first = trace->begin();
				auto trace_last = trace->end();
				--trace_last;
				if (reversed)
					std::swap(trace_first, trace_last);
				for (auto it = trace_first; it != trace_last;
				     !reversed ? ++it : --it)
				{
					neutral_vert_tet nv_tet;
					if (last_sing_neutral_v == (size_t) -1)
					{
						nv_tet.neutral_v[0] = it->neutral_v;
						nv_tet.neutral_v[1] = -1;
					}
					else
					{
						nv_tet.neutral_v[0] = last_sing_neutral_v;
						nv_tet.neutral_v[1] = it->neutral_v;
						nv_tet.next_v = tet_verts.size() + 1;
						last_sing_neutral_v = -1;
					}
					tet_verts.push_back(nv_tet);
				}

				if (tet_face_actual_sing(tet_index, cur_face_info,
				                         *trace_last))
					last_sing_neutral_v = trace_last->neutral_v;

				if (traces_fit(t, tet_index,
				               *trace, cur_face_info, reversed,
				               start_trace, start_face_info, false))
				{
					if (last_sing_neutral_v != (size_t) -1)
					{
						auto& first_v = tet_verts[first_v_index];
						first_v.neutral_v[1] = first_v.neutral_v[0];
						first_v.neutral_v[0] = last_sing_neutral_v;
						first_v.next_v = first_v_index + 1;
					}

					if (tet_verts.back().neutral_v[1] != (size_t) -1)
						tet_verts.back().next_v = first_v_index;

					ti.boundary_ends.push_back(tet_verts.size());
					break;
				}

				// Find the next trace.
				// TODO: Try using a std::map to speed this up.
				bool found = false;
				for (unsigned int next_face = 0; next_face < 4; next_face++)
				{
					const auto& next_face_intersections =
						face_infos[t.faces[next_face]].intersections;
					for (size_t next_trace_i = 0;
					     next_trace_i < next_face_intersections.size();
					     next_trace_i++)
					{
						if (used_traces[next_face][next_trace_i])
							continue;
						const auto& next_trace =
							next_face_intersections[next_trace_i];

						bool next_reversed = false;
						do
						{
							if (traces_fit(t, tet_index,
							               *trace, cur_face_info, reversed,
							               next_trace,
							               face_infos[t.faces[next_face]],
							               next_reversed))
							{
								found = true;
								face = next_face;
								trace_i = next_trace_i;
								trace = &next_trace;
								reversed = next_reversed;
								break;
							}

							next_reversed = !next_reversed;
						} while (next_reversed != false);

						if (found)
							break;
					}

					if (found)
						break;
				}
				assert(found);
			}
		}
	}
}

bool neutral_tracer_param::traces_fit(
	const tet& tet, size_t tet_index,
	const face_info::trace& t0, const face_info& t0_face_info, bool reversed0,
	const face_info::trace& t1, const face_info& t1_face_info, bool reversed1)
{
	const auto& v0 = !reversed0 ? t0.back() : t0.front();
	const auto& v1 = !reversed1 ? t1.front() : t1.back();

	// Not singularities in this tet: check if they are the same point.
	if (!tet_face_actual_sing(tet_index, t0_face_info, v0))
		return v0.neutral_v == v1.neutral_v;

	// 1 singularity 1 non-singularity: don't fit.
	else if (!tet_face_actual_sing(tet_index, t1_face_info, v1))
		return false;

	// Wrong singularity on same tet: ditto.
	else if ((v0.flag & 1) != (v1.flag & 1))
		return false;

	// Same singularity: check if they are on the same side of the sphere.
	else
	{
		// Use adjacent vertices to check which side, as it is hard to check for
		// the singularities.
		Vector3ft adj_mid[2] = {
			neutral_verts[t0[!reversed0 ? t0.size() - 2 : 1].neutral_v].mid,
			neutral_verts[t1[!reversed1 ? 1 : t1.size() - 2].neutral_v].mid
		};

		Vector3ft mid[2];
		for (unsigned int i = 0; i < 2; i++)
		{
			if (tet_mid_has_wrong_sign(tet, adj_mid[i]))
				adj_mid[i] = -adj_mid[i];

			mid[i] = neutral_verts[(i ? v1 : v0).neutral_v].mid;
			if (mid[i].dot(adj_mid[i]) < 0.0)
				mid[i] = -mid[i];
		}

		return mid[0].dot(mid[1]) >= 0.0;
	}
}

void neutral_tracer_param::add_interior_verts(
	size_t tet_index, const tet& t, tet_info& ti, float_type f, float_type f_sq,
	const Matrix3ft& rot, const std::vector<Vector3ft_na>& tet_mids,
	std::vector<Vector2ft_na>& points_out, const triangulate::triangle& tri)
{
	auto is_sing = [&](unsigned int i) -> bool
	{
		return ti.tet_verts[tri[i]].neutral_v[1] != (size_t) -1;
	};

	// Find a non-singularity vertex to be v0. This should always exist because
	// otherwise the triangle would have an edge of length pi.
	unsigned int nonsing;
	for (nonsing = 0; nonsing < 3; nonsing++)
		if (!is_sing(nonsing))
			break;
	assert(nonsing != 3);

	Vector3ft tri_mids[3];
	Vector2ft ellipse_param[3];
	for (unsigned int i = 0; i < 3; i++)
	{
		Vector3ft mid = tet_mids[tri[(nonsing + i) % 3]];
		tri_mids[i] = mid;
		ellipse_param[i] = neutral_tracer::elliptic_inv_param(
			f, f_sq, ti.eigenvectors.transpose() * mid);
	}

	// Adjust branch cuts so that the triangle doesn't cross any. The branch
	// cuts are on the great circle orthogonal to the cokernel's medium
	// eigenvector. This could be considered lifting to a branched covering
	// space. The triangle is guaranteed not to cover a branch point.

	Vector3ft mid0 = tri_mids[0];
	bool mid0_side = mid0.dot(ti.eigenvectors.col(1)) >= 0.0;
	for (unsigned int i = 1; i < 3; i++)
	{
		if (is_sing((i + nonsing) % 3))
		{
			ellipse_param[i].y() = ellipse_param[i].y() > M_PI_2 ? M_PI : 0.0;

			if (ellipse_param[i].x() > M_PI_2 &&
			    ellipse_param[i].x() < 3 * M_PI_2)
				// Already in middle.
				continue;

			// Just connect the shortest way with v0.
			ellipse_param[i].x() = mid0_side ? 0.0 : 2 * M_PI;
			continue;
		}

		Vector3ft mid = tri_mids[i];
		bool side = mid.dot(ti.eigenvectors.col(1)) >= 0.0;

		if (side == mid0_side)
			// No crossing.
			continue;

		// See where it crossed.
		Vector3ft edge = mid0.cross(mid);
		bool sing_sides[2];
		for (unsigned int j = 0; j < 2; j++)
			sing_sides[j] = edge.dot(ti.sings[j]) >= 0.0;

		// +, + : Right
		// -, - : Left
		// +, - : Up on front (mid0_side == true), down on back.
		// -, + : Down on front, up on back.

		// Note: Up on the sphere (increasing in the major eigenvector) is down
		// on the parameter space since it is getting further from the +
		// singularities. The table above is relative to looking at the front of
		// the sphere, where the minor eigenvector is pointing left and the
		// major eigenvector up, looking down the medium eigenvector.

		if (mid0_side && sing_sides[0] && sing_sides[1] ||
		    !mid0_side && !sing_sides[0] && !sing_sides[1])
			// Didn't cross over branch cut.
			continue;

		if (sing_sides[0] && sing_sides[1])
			ellipse_param[i][0] += 2 * M_PI;
		else if (!sing_sides[0] && !sing_sides[1])
			ellipse_param[i][0] -= 2 * M_PI;
		else // sing_sides[0] != sing_sides[1]
		{
			bool up = mid0_side ^ sing_sides[1];
			float_type y_shift = up ? 0.0 : 2 * M_PI;
			ellipse_param[i] =
				Vector2ft(2 * M_PI, y_shift) - ellipse_param[i];
		}
	}

	sample_elliptic_triangle(tet_index, t, ti, f_sq, rot, points_out,
	                         ellipse_param);
}

void neutral_tracer_param::sample_elliptic_triangle(
	size_t tet_index, const tet& t, tet_info& ti, float_type f_sq,
	const Matrix3ft& rot, std::vector<Vector2ft_na>& points_out,
	Vector2ft (&tri)[3])
{
	auto tet_loc = get_tet_loc(t);

	const float_type x_inc = M_PI / elliptic_x_cells;
	const float_type y_inc = M_PI / elliptic_y_cells;
	const float_type x_inc_inv = 1.0 / x_inc;
	const float_type y_inc_inv = 1.0 / y_inc;

//	Matrix3ft tri_loc;
//	for (unsigned int k = 0; k < 3; k++)
//		tri_loc.col(k) = tri[k].homogeneous();
//	for (size_t i = 0; i < 4 * elliptic_x_cells; i++)
//	{
//		for (size_t j = 0; j < 2 * elliptic_y_cells; j++)
//		{
//			Vector2ft param(x_inc * j, y_inc * i);
//			Vector3ft tri_bary = tri_loc.inverse() * param.homogeneous();
//			if ((tri_bary.array() < 0.0).any())
//				continue;
//
//			Vector3ft mid = ti.eigenvectors *
//				neutral_tracer::elliptic_param(f_sq - 1.0, param);
//			Vector4ft bary = tet_mid_to_bary(t, mid);
//			if ((bary.array() < 0.0).any())
//				continue;
//
//			ti.tet_verts.emplace_back();
//			auto& tv = ti.tet_verts.back();
//			tv.neutral_v[0] = neutral_verts.size();
//			tv.neutral_v[1] = (size_t) -1;
//
//			neutral_verts.emplace_back();
//			auto& nv = neutral_verts.back();
//			nv.loc = tet_loc * barycentric_normalized(bary);
//			nv.mid = mid;
//			nv.index[0] = tet_index;
//
//			points_out.push_back(to_stereographic(rot.transpose() * mid));
//		}
//	}
//	return;

	for (unsigned int i = 0; i < 3; i++)
	{
		tri[i].x() *= x_inc_inv;
		tri[i].y() *= y_inc_inv;
	}

	// Sort by y coordinate: ascending order.
	if (tri[0].y() > tri[1].y())
		std::swap(tri[0], tri[1]);
	if (tri[1].y() > tri[2].y())
		std::swap(tri[1], tri[2]);
	if (tri[0].y() > tri[1].y())
		std::swap(tri[0], tri[1]);

	// Split align horizontal line going through tri[1].
	float_type flat_y = tri[1].y();
	float_type split_t = (tri[1].y() - tri[0].y()) / (tri[2].y() - tri[0].y());
	float_type split_x = split_t * tri[2].x() + (1.0 - split_t) * tri[0].x();

	float_type x0 = tri[1].x();
	float_type x1 = split_x;
	if (x0 > x1)
		std::swap(x0, x1);

	auto draw_aligned_tri =
	[&](const Vector2ft& tip, bool point_up)
	{
		float_type min_y = point_up ? flat_y : tip.y();
		float_type max_y = point_up ? tip.y() : flat_y;
		size_t start_y = (size_t) std::ceil(min_y);
		if (start_y > max_y)
			return;

		float_type slope_0 = (tip.x() - x0) / (tip.y() - flat_y);
		float_type slope_1 = (tip.x() - x1) / (tip.y() - flat_y);

		size_t height = (size_t) (max_y - start_y);
		float_type left = point_up ? x0 : tip.x();
		float_type right = point_up ? x1 : tip.x();
		left += slope_0 * (start_y - min_y);
		right += slope_1 * (start_y - min_y);
		for (size_t i = 0; i <= height;
		     i++, left += slope_0, right += slope_1)
		{
			size_t start_x = (size_t) std::ceil(left);
			if (start_x > right)
				continue;

			size_t width = (size_t) (right - start_x);
			for (size_t j = 0; j <= width; j++)
			{
				size_t x = start_x + j;
				size_t y = start_y + i;

				// Don't duplicate the singularities.
				if (x % elliptic_x_cells == 0 && y % elliptic_y_cells == 0)
					continue;

				Vector2ft param(x_inc * x, y_inc * y);
				Vector3ft mid = ti.eigenvectors *
					neutral_tracer::elliptic_param(f_sq - 1.0, param);
				Vector4ft bary = tet_mid_to_bary(t, mid);
				if ((bary.array() < 0.0).any())
				{
					// These aren't necessarily a problem as the neutral surface
					// doesn't have to match the approximate boundaries exactly.
					// However, it does indicate a problem if there are too many
					// of them.

					// std::cout << "Bad point: " << param.transpose() << '\n';
					continue;
				}

				ti.tet_verts.emplace_back();
				auto& tv = ti.tet_verts.back();
				tv.neutral_v[0] = neutral_verts.size();
				tv.neutral_v[1] = (size_t) -1;

				neutral_verts.emplace_back();
				auto& nv = neutral_verts.back();
				nv.loc = tet_loc * barycentric_normalized(bary);
				nv.mid = mid;
				// Don't need to normalize, as it will be later.
				nv.normal = mid_evec_to_normal(t, ti, mid);
				nv.index[0] = tet_index;

				points_out.push_back(to_stereographic(rot.transpose() * mid));
			}
		}
	};

	draw_aligned_tri(tri[0], false);
	draw_aligned_tri(tri[2], true);
}

bool neutral_tracer_param::tet_face_actual_sing(
	size_t tet_index, const face_info& fi, const neutral_vert_tetface& v)
{
	return neutral_verts[v.neutral_v].kind == SINGULARITY &&
		fi.tet[v.flag >> 1] == tet_index;
}

Eigen::Matrix<float_type, 3, 4> neutral_tracer_param::get_tet_loc(const tet& t)
{
	Eigen::Matrix<float_type, 3, 4> loc;
	for (unsigned int j = 0; j < 4; j++)
		loc.col(j) = vertices[t.verts[j]].loc;
	return loc;
}

Eigen::Matrix<float_type, 5, 4>
neutral_tracer_param::get_tet_field(const tet& t)
{
	Eigen::Matrix<float_type, 5, 4> field;
	for (unsigned int j = 0; j < 4; j++)
		field.col(j) = vertices[t.verts[j]].field;
	return field;
}

Eigen::Matrix<float_type, 3, 3>
neutral_tracer_param::get_face_loc(const face& f)
{
	Eigen::Matrix<float_type, 3, 3> loc;
	for (unsigned int j = 0; j < 3; j++)
		loc.col(j) = vertices[f.verts[j]].loc;
	return loc;
}

Eigen::Matrix<float_type, 5, 3>
neutral_tracer_param::get_face_field(const face& f)
{
	Eigen::Matrix<float_type, 5, 3> field;
	for (unsigned int j = 0; j < 3; j++)
		field.col(j) = vertices[f.verts[j]].field;
	return field;
}

Eigen::Matrix<float_type, 3, 2>
neutral_tracer_param::get_edge_loc(const edge& e)
{
	Eigen::Matrix<float_type, 3, 2> loc;
	for (unsigned int j = 0; j < 2; j++)
		loc.col(j) = vertices[e[j]].loc;
	return loc;
}

Eigen::Matrix<float_type, 5, 2>
neutral_tracer_param::get_edge_field(const edge& e)
{
	Eigen::Matrix<float_type, 5, 2> field;
	for (unsigned int j = 0; j < 2; j++)
		field.col(j) = vertices[e[j]].field;
	return field;
}

Vector3ft neutral_tracer_param::face_mid_to_bary(
	const face& f, const Vector3ft& mid) const
{
	Matrix3ft field_mid_mat;
	for (unsigned int i = 0; i < 3; i++)
		field_mid_mat.col(i) =
			tensor3_to_matrix(vertices[f.verts[i]].field) * mid;
	return barycentric_normalized(get_kernel<1>(field_mid_mat));
}

Eigen::Matrix<float_type, 3, 2> neutral_tracer_param::sample_face_param(
	const Eigen::Matrix<float_type, 5, 2>& cokernel,
	Eigen::Quaternion<float_type>& q,
	float_type theta)
{
	tensor3 comb_cokernel = cokernel * Vector2ft(cos(theta), sin(theta));

	Eigen::SelfAdjointEigenSolver<Matrix3ft>
		eig(tensor3_to_quadratic_eq(comb_cokernel));
	Matrix3ft eigenvectors = eig.eigenvectors();
	if (eigenvectors.determinant() < 0.0)
		eigenvectors = -eigenvectors;

	Eigen::Quaternion<float_type> q_new(eigenvectors);
	q_new = q_new * quaternion_group::closest_in_left_coset(q_new, q)
		.to_eigen<float_type>();
	q = q_new;

	// Find the two points, which are the singularities of comb_cokernel.

	float_type min = eig.eigenvalues()[0];
	float_type mid = eig.eigenvalues()[1];
	float_type max = eig.eigenvalues()[2];

	Vector2ft normal0 =
		Vector2ft(mid - min, max - mid).array().sqrt() / sqrt(max - min);
	Eigen::Matrix<float_type, 3, 2> samples;
	samples.col(0) = q * Vector3ft(normal0[0], 0.0, normal0[1]);
	samples.col(1) = q * Vector3ft(normal0[0], 0.0, -normal0[1]);
	return samples;
}

void neutral_tracer_param::reorder_face_sample(
	face_param_starting_vert& sv, const Eigen::Matrix<float_type, 3, 2>& sample)
{
	// This is always the index of a present vert.
	unsigned int i = !sv.present[0];

	if (rp2_dist(neutral_verts[sv.v[i].neutral_v].mid, sample.col(!i)) <
	    rp2_dist(neutral_verts[sv.v[i].neutral_v].mid, sample.col(i)))
	{
		std::swap(sv.v[0], sv.v[1]);
		std::swap(sv.present[0], sv.present[1]);
	}
}

Eigen::Matrix<float_type, 3, 4>
neutral_tracer_param::get_field_mid_mat(
	const tet& tet, const Vector3ft& mid) const
{
	Eigen::Matrix<float_type, 3, 4> field_mid_mat;
	for (unsigned int i = 0; i < 4; i++)
		field_mid_mat.col(i) =
			tensor3_to_matrix(vertices[tet.verts[i]].field) * mid;
	return field_mid_mat;
}

Vector4ft
neutral_tracer_param::tet_mid_to_bary(const tet& t, const Vector3ft& mid) const
{
	auto out = neutral_tracer::field_mid_to_loc(get_field_mid_mat(t, mid));
//	static float_type min_len = 1e100;
//	if (min_len > out.norm())
//	{
//		min_len = out.norm();
//		std::cout << "min_len = " << min_len << '\n';
//	}
	return out;
}

Vector3ft neutral_tracer_param::mid_evec_to_normal(const tet& t,
	const tet_info& ti, const Vector3ft& mid) const
{
	return neutral_tracer::mid_evec_to_normal(
		get_field_mid_mat(t, mid), ti.loc_inv, mid);
}

bool neutral_tracer_param::tet_mid_has_wrong_sign(
	const tet& tet, const Vector3ft& mid) const
{
	return tet_mid_to_bary(tet, mid).sum() < 0.0;
}

Vector2ft neutral_tracer_param::to_stereographic(const Vector3ft& x)
{
	// Mirror output since otherwise stereographic projection would reverse the
	// winding order.
	return Vector2ft(x[0], -x[1]) / (1.0 - x[2]);
}

Vector3ft neutral_tracer_param::from_stereographic(const Vector2ft& x)
{
	float_type recip = 2.0 / (1.0 + x.squaredNorm());

	Vector3ft out;
	out << x[0], -x[1], -1.0;

	return recip * out + Vector3ft::UnitZ();
}

void neutral_tracer_param::init_gl(const char* shader_prefix)
{
	glGenBuffers(1, &lighting_buffer);
	glGenBuffers(1, &neutral_surface_buffer);
	glGenBuffers(1, &neutral_index_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, neutral_surface_buffer);
	{
		glBufferData(
			GL_ARRAY_BUFFER,
			neutral_verts.size() * sizeof(neutral_surface_vattrs),
			NULL, GL_STATIC_DRAW);
		neutral_surface_vattrs* attrs = (neutral_surface_vattrs*)
			glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		for (const auto& p : neutral_verts)
		{
			Eigen::Map<Eigen::Matrix<GLfloat, 4, 1> >(attrs->loc) <<
				p.loc.cast<GLfloat>(), 1.0;
			//	p.mid.cast<GLfloat>(), 1.0;
			Eigen::Map<Eigen::Matrix<GLfloat, 4, 1> >(attrs->mid) <<
				p.mid.cast<GLfloat>(), 1.0;
			Eigen::Map<Eigen::Matrix<GLfloat, 3, 1> >(attrs->normal) =
				p.normal.cast<GLfloat>();
			//	mid.cast<GLfloat>();
			attrs++;
		}

		glUnmapBuffer(GL_ARRAY_BUFFER);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, neutral_index_buffer);
	{
		// Write the data to the buffer.
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		             neutral_triangles.size() * 3 * sizeof(GLuint),
		             NULL, GL_STATIC_DRAW);
		GLuint* attrs = (GLuint*)
			glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

		for (const auto& tri : neutral_triangles)
		{
			*(attrs++) = tri.neutral_v[0];
			*(attrs++) = tri.neutral_v[1];
			*(attrs++) = tri.neutral_v[2];
		}

		glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_UNIFORM_BUFFER, lighting_buffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(light_buf), NULL, GL_STREAM_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	std::string vert_file = std::string(shader_prefix) + "neutral.vert";
	std::string frag_file = std::string(shader_prefix) + "neutral.frag";
	neutral_shader_program = create_shader_program(
		vert_file.c_str(),
		frag_file.c_str());
	neutral_MVP_uniform = glGetUniformLocation(neutral_shader_program, "MVP");
	neutral_lighting_uniform =
		glGetUniformBlockIndex(neutral_shader_program, "lightBuf");
	glUniformBlockBinding(neutral_shader_program, neutral_lighting_uniform, 0);

	glGenVertexArrays(1, &neutral_vao);

	glBindVertexArray(neutral_vao);
	glBindBuffer(GL_ARRAY_BUFFER, neutral_surface_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, neutral_index_buffer);
	glEnableVertexAttribArray(neutral_loc_attr);
	glVertexAttribPointer(neutral_loc_attr, 4, GL_FLOAT, GL_FALSE,
	                      sizeof(neutral_surface_vattrs),
	                      (void*) offsetof(neutral_surface_vattrs, loc));
	glEnableVertexAttribArray(neutral_mid_attr);
	glVertexAttribPointer(neutral_mid_attr, 4, GL_FLOAT, GL_FALSE,
	                      sizeof(neutral_surface_vattrs),
	                      (void*) offsetof(neutral_surface_vattrs, mid));
	glEnableVertexAttribArray(neutral_normal_attr);
	glVertexAttribPointer(neutral_normal_attr, 3, GL_FLOAT, GL_FALSE,
	                      sizeof(neutral_surface_vattrs),
	                      (void*) offsetof(neutral_surface_vattrs, normal));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void neutral_tracer_param::render(const Matrix4gf& mvp, bool wireframe)
{
	glUseProgram(neutral_shader_program);
	glUniformMatrix4fv(neutral_MVP_uniform, 1, GL_FALSE, mvp.data());

	glEnable(GL_CLIP_DISTANCE0);
	glEnable(GL_CLIP_DISTANCE1);
	glEnable(GL_CLIP_DISTANCE2);
	glEnable(GL_CLIP_DISTANCE3);

	glPolygonMode(GL_FRONT_AND_BACK, wireframe ? GL_LINE : GL_FILL);
	glLineWidth(2.0f);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, lighting_buffer);

	glBindVertexArray(neutral_vao);
	glDrawElements(GL_TRIANGLES, 3 * neutral_triangles.size(),
	               GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);

	glDisable(GL_CLIP_DISTANCE0);
	glDisable(GL_CLIP_DISTANCE1);
	glDisable(GL_CLIP_DISTANCE2);
	glDisable(GL_CLIP_DISTANCE3);

	glUseProgram(0);
}

neutral_tracer_param::light_buf neutral_tracer_param::get_lighting()
{
	light_buf lighting_out;
	glBindBuffer(GL_UNIFORM_BUFFER, lighting_buffer);
	light_buf* lighting = (light_buf*)
		glMapBuffer(GL_UNIFORM_BUFFER, GL_READ_ONLY);
	lighting_out = *lighting;
	glUnmapBuffer(GL_UNIFORM_BUFFER);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	return lighting_out;
}

void neutral_tracer_param::set_lighting(light_buf lighting)
{
	glBindBuffer(GL_UNIFORM_BUFFER, lighting_buffer);
	light_buf* lighting_map = (light_buf*)
		glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
	*lighting_map = lighting;
	glUnmapBuffer(GL_UNIFORM_BUFFER);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}
