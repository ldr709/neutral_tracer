#ifndef QUATERNION_GROUP_H
#define QUATERNION_GROUP_H

#include <inttypes.h>
#include <Eigen/Dense>
#include <iostream>

class quaternion_group
{
private:
	enum class quaternion_group_enum : uint8_t
	{
		I,
		J,
		K,
		ONE,
		MINUS_I,
		MINUS_J,
		MINUS_K,
		MINUS_ONE
	};

	quaternion_group_enum value;
	quaternion_group(quaternion_group_enum x) : value(x) {}

public:
	quaternion_group() : value(quaternion_group_enum::ONE) {}

	friend bool operator==(quaternion_group a, quaternion_group b);
	friend bool operator!=(quaternion_group a, quaternion_group b);
	friend quaternion_group operator*(quaternion_group a, quaternion_group b);

	quaternion_group& operator*=(quaternion_group x)
	{
		*this = *this * x;
		return *this;
	}

	quaternion_group inverse() const;

	quaternion_group conjugate() const
	{
		return inverse();
	}

	template<typename T>
	Eigen::Matrix<T, 4, 4> left_multiplication_matrix() const;

	template<typename T>
	Eigen::Matrix<T, 4, 4> right_multiplication_matrix() const;

	template<typename T>
	static quaternion_group closest_to(const Eigen::Quaternion<T>& x);

	template<typename T>
	static quaternion_group
	closest_in_left_coset(const Eigen::Quaternion<T>& x,
	                      const Eigen::Quaternion<T>& closest_to);

	static quaternion_group identity()
	{
		return quaternion_group(quaternion_group_enum::ONE);
	}

	static quaternion_group i()
	{
		return quaternion_group(quaternion_group_enum::I);
	}

	static quaternion_group j()
	{
		return quaternion_group(quaternion_group_enum::J);
	}

	static quaternion_group k()
	{
		return quaternion_group(quaternion_group_enum::K);
	}

	static quaternion_group one()
	{
		return quaternion_group(quaternion_group_enum::ONE);
	}

	static quaternion_group minus_i()
	{
		return quaternion_group(quaternion_group_enum::MINUS_I);
	}

	static quaternion_group minus_j()
	{
		return quaternion_group(quaternion_group_enum::MINUS_J);
	}

	static quaternion_group minus_k()
	{
		return quaternion_group(quaternion_group_enum::MINUS_K);
	}

	static quaternion_group minus_one()
	{
		return quaternion_group(quaternion_group_enum::MINUS_ONE);
	}

	template<typename T>
	Eigen::Quaternion<T> to_eigen() const
	{
		uint8_t x = static_cast<uint8_t>(value);
		Eigen::Matrix<T, 4, 1> q = Eigen::Matrix<T, 4, 1>::Unit(x & 3);
		if (x & 4)
			q = -q;
		return Eigen::Quaternion<T>(q);
	}

	template<typename T>
	operator Eigen::Quaternion<T>() const
	{
		return to_eigen<T>();
	}

	friend std::ostream& operator<<(std::ostream& stream, quaternion_group x);
};

inline bool operator==(quaternion_group a, quaternion_group b)
{
	return a.value == b.value;
}

inline bool operator!=(quaternion_group a, quaternion_group b)
{
	return !(a == b);
}

inline quaternion_group operator*(quaternion_group a, quaternion_group b)
{
	uint8_t a_int = static_cast<uint8_t>(a.value);
	uint8_t b_int = static_cast<uint8_t>(b.value);

	// Bitwise magic.
	uint8_t out = a_int ^ b_int ^ 3;
	out ^= ((~b_int & a_int + 1) + 1 & 2) << 1;
	return quaternion_group(static_cast<quaternion_group::quaternion_group_enum>(out));
}

inline quaternion_group quaternion_group::inverse() const
{
	uint8_t x = static_cast<uint8_t>(value);
	x ^= (x & 3) != 3 ? 4 : 0;
	return quaternion_group(static_cast<quaternion_group_enum>(x));
}

// Converts the quaternion to a matrix that can be multiplied by a quaternion
// to get the result of (this->to_eigen<T>() * q).
template<typename T>
inline Eigen::Matrix<T, 4, 4> quaternion_group::left_multiplication_matrix() const
{
	Eigen::Matrix<T, 4, 4> out;
	uint8_t x_int = static_cast<uint8_t>(value);
	switch (x_int & 3)
	{
	case 0:
		out << 0,  0,  0,  1,
		       0,  0, -1,  0,
		       0,  1,  0,  0,
		      -1,  0,  0,  0;
		break;

	case 1:
		out << 0,  0,  1,  0,
		       0,  0,  0,  1,
		      -1,  0,  0,  0,
		       0, -1,  0,  0;
		break;

	case 2:
		out << 0, -1,  0,  0,
		       1,  0,  0,  0,
		       0,  0,  0,  1,
		       0,  0, -1,  0;
		break;

	case 3:
		out = Eigen::Matrix<T, 4, 4>::Identity();
	}

	if (x_int & 4)
		out = -out;
	return out;
}

// Converts the quaternion to a matrix that can be multiplied by a quaternion
// to get the result of (q * this->to_eigen<T>()).
template<typename T>
inline Eigen::Matrix<T, 4, 4> quaternion_group::right_multiplication_matrix() const
{
	Eigen::Matrix<T, 4, 4> out;
	uint8_t x_int = static_cast<uint8_t>(value);
	switch (x_int & 3)
	{
	case 0:
		out << 0,  0,  0,  1,
		       0,  0,  1,  0,
		       0, -1,  0,  0,
		      -1,  0,  0,  0;
		break;

	case 1:
		out << 0,  0, -1,  0,
		       0,  0,  0,  1,
		       1,  0,  0,  0,
		       0, -1,  0,  0;
		break;

	case 2:
		out << 0,  1,  0,  0,
		      -1,  0,  0,  0,
		       0,  0,  0,  1,
		       0,  0, -1,  0;
		break;

	case 3:
		out = Eigen::Matrix<T, 4, 4>::Identity();
	}

	if (x_int & 4)
		out = -out;
	return out;
}

// Find the closest member of the quaternion group to x.
template<typename T>
inline quaternion_group
quaternion_group::closest_to(const Eigen::Quaternion<T>& x)
{
	Eigen::Vector4d x_abs = x.coeffs().cwiseAbs();
	int max_index = 0;
	for (int i = 1; i < 4; i++)
		if (x_abs[i] >= x_abs[max_index])
			max_index = i;
	if (x.coeffs()[max_index] < 0.0)
		max_index |= 4;
	return quaternion_group(static_cast<quaternion_group_enum>(max_index));
}

// Find the closest quaternion to closest_to in x's left coset of the
// quaternion group. Return the transformation from x to this quaternion,
// as a member of the quaternion group.
template<typename T>
inline quaternion_group
quaternion_group::closest_in_left_coset(const Eigen::Quaternion<T>& x,
                                        const Eigen::Quaternion<T>& closest_to)
{
	return quaternion_group::closest_to(x.conjugate() * closest_to);
}

#endif
