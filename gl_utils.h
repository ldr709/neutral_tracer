#ifndef TENSOR_LINES_GL_UTILS_H
#define TENSOR_LINES_GL_UTILS_H

#include <GL/glew.h>
#include <Eigen/Dense>

typedef Eigen::Matrix<GLfloat, 1, 1> Vector1gf;
typedef Eigen::Matrix<GLfloat, 2, 1> Vector2gf;
typedef Eigen::Matrix<GLfloat, 3, 1> Vector3gf;
typedef Eigen::Matrix<GLfloat, 4, 1> Vector4gf;
typedef Eigen::Matrix<GLfloat, 5, 1> Vector5gf;
typedef Eigen::Matrix<GLfloat, 6, 1> Vector6gf;
typedef Eigen::Matrix<GLfloat, 7, 1> Vector7gf;
typedef Eigen::Matrix<GLfloat, 8, 1> Vector8gf;

typedef Eigen::Matrix<GLfloat, 1, 1> Matrix1gf;
typedef Eigen::Matrix<GLfloat, 2, 2> Matrix2gf;
typedef Eigen::Matrix<GLfloat, 3, 3> Matrix3gf;
typedef Eigen::Matrix<GLfloat, 4, 4> Matrix4gf;

typedef Eigen::Transform<GLfloat, 3, Eigen::Affine> Transform3gfa;
typedef Eigen::Transform<GLfloat, 3, Eigen::Projective> Transform3gf;

std::string read_shader(const char* path);
GLuint compile_shader(const char* path, GLenum type);
GLuint create_shader_program(const char* vertex_shader_file,
                             const char* frag_shader_file,
                             const char* geom_shader_file = NULL);

#endif
