#include <Eigen/Dense>

#ifndef TENSOR_LINES_NEUTRAL_QR_H
#define TENSOR_LINES_NEUTRAL_QR_H

namespace Eigen
{

// If A = QR is m x n, given a matrix with m rows b, find (A*^-1) b. Actually
// uses A's pseudoinverse, so this still works when A is not square. Does this
// by calculating y such that R* y = x, then returns Q y.
template<typename M, typename Lhs, typename Rhs>
void solveAdjoint(MatrixBase<Lhs>& dst, const HouseholderQR<M>& qr,
                  const MatrixBase<Rhs>& b)
{
	eigen_assert(dst.rows() == qr.rows());
	eigen_assert(b.rows() == qr.cols());

	typename M::Index rank = qr.cols();
	dst.topRows(rank) = qr
		.matrixQR()
		.topLeftCorner(rank, rank)
		.template triangularView<Upper>()
		.adjoint()
		.solve(b);
	dst.bottomRows(dst.rows() - rank).setZero();
	dst.applyOnTheLeft(qr.householderQ());
}

// Solves on the right. b * A^-1.
template<typename M, typename Lhs, typename Rhs>
void solveRight(MatrixBase<Lhs>& dst, const HouseholderQR<M>& qr,
                const MatrixBase<Rhs>& b)
{
	eigen_assert(dst.cols() == qr.rows());
	eigen_assert(b.cols() == qr.cols());

	typename M::Index rank = qr.cols();
	dst.leftCols(rank) = qr
		.matrixQR()
		.topLeftCorner(rank, rank)
		.template triangularView<Upper>()
		.template solve<OnTheRight>(b);
	dst.rightCols(dst.cols() - rank).setZero();
	dst.applyOnTheRight(qr.householderQ().adjoint());
}

}

#endif
