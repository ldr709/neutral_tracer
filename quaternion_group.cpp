#include "quaternion_group.h"

static const char quaternion_group_element_names[] = { 'i', 'j', 'k', '1' };

std::ostream& operator<<(std::ostream& stream, quaternion_group x)
{
	uint8_t x_int = static_cast<uint8_t>(x.value);

	char output[3] = { 0 };
	int i = 0;
	if (x_int & 4)
		output[i++] = '-';
	else if (stream.flags() & std::ios_base::showpos)
		output[i++] = '+';
	output[i] = quaternion_group_element_names[x_int & 3];

	return stream << output;
}
