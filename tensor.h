#ifndef TENSOR_LINES_TENSOR_H
#define TENSOR_LINES_TENSOR_H

#include <iostream>
#include <list>
#include <memory>
#include <random>

#include <Eigen/Dense>

typedef double float_type;

typedef Eigen::Matrix<float_type, 1, 1> Vector1ft;
typedef Eigen::Matrix<float_type, 2, 1> Vector2ft;
typedef Eigen::Matrix<float_type, 3, 1> Vector3ft;
typedef Eigen::Matrix<float_type, 4, 1> Vector4ft;
typedef Eigen::Matrix<float_type, 5, 1> Vector5ft;
typedef Eigen::Matrix<float_type, 6, 1> Vector6ft;
typedef Eigen::Matrix<float_type, 7, 1> Vector7ft;
typedef Eigen::Matrix<float_type, 8, 1> Vector8ft;

using Eigen::Vector3i;

// Disable alignment
typedef Eigen::Matrix<float_type, 2, 1, Eigen::DontAlign> Vector2ft_na;
typedef Eigen::Matrix<float_type, 3, 1, Eigen::DontAlign> Vector3ft_na;
typedef Eigen::Matrix<float_type, 4, 1, Eigen::DontAlign> Vector4ft_na;

typedef Eigen::Matrix<float_type, 1, 1> Matrix1ft;
typedef Eigen::Matrix<float_type, 2, 2> Matrix2ft;
typedef Eigen::Matrix<float_type, 3, 3> Matrix3ft;
typedef Eigen::Matrix<float_type, 4, 4> Matrix4ft;

// Only 5 independent values in a 3x3 symmetric traceless tensor.
// Order: tensor[0] tensor[1] tensor[2]
//        tensor[1] tensor[3] tensor[4]
//        tensor[2] tensor[4] -tensor[0]-tensor[3]
const size_t tensor_size = 5;

// A symmetric real 3x3 tensor.
typedef Eigen::Matrix<float_type, tensor_size, 1> tensor3;
typedef Eigen::Matrix<float_type, tensor_size, 1, Eigen::DontAlign> tensor3_na;

// Affine tensor field in dim dimensions. Can be used as barycentric or affine.
// If the field is affine, the last column is the field at the origin. Multiply
// by the location in homogeneous coordinates to get the field. For 3D affine,
// the order is x, y, z, 0.
// Can also be used for barycentric fields.
template<unsigned int dim>
using affine_field = Eigen::Matrix<float_type, tensor_size, dim + 1>;

// Each entry represents one degenerate point.
struct degen_point
{
	// 3D coordinates.
	Vector4ft_na loc;

	Vector3ft_na nondegen_vec;

	// Index of tet or face.
	size_t index;

	bool on_face;
};

namespace detail
{
	struct detail_tetrahedron
	{
		Eigen::Matrix<float_type, 3, 4, Eigen::DontAlign> loc;
		Eigen::Matrix<float_type, 5, 4, Eigen::DontAlign> field;
	};
}

// A line segment between two degenerate points. The points are referenced by
// indices.
struct degen_line_segment
{
	size_t first;
	size_t second;

	degen_line_segment(size_t first_in, size_t second_in) :
		first(first_in),
		second(second_in) {}
};

class degenerate_curve_tracer
{
public:
	// bool is true when this curve is a loop.
	typedef std::pair<std::vector<degen_point>, bool> degen_curve;

	struct vertex
	{
		Vector3ft_na loc;
		tensor3_na field;
	};

	struct face
	{
		size_t verts[3];
	};

	struct tet
	{
		size_t verts[4];
		size_t faces[4];
	};

	struct degen_curve_head
	{
		degen_point pnt;
		// Unordered (i.e., reversible) linked list.
		degen_curve_head* link[2];
		bool visited = false;
	};

private:
	static void attach_lists(degen_curve_head* h0, degen_curve_head* h1);
	static void advance_head(degen_curve_head*& h, degen_curve_head*& p);

	typedef detail::detail_tetrahedron tet_internal;
	struct face_internal
	{
		Matrix3ft loc;
		affine_field<2> field;
	};

	const std::vector<vertex>& vertices;
	const std::vector<face>& faces;
	const std::vector<tet>& tets;

	typedef std::vector<degen_curve_head*> face_ints_info;
	std::vector<face_ints_info> face_ints;

	std::vector<degen_curve_head*> degenerate_curves;

	void find_face_degenerates(
		size_t face_i, std::vector<degen_curve_head*>& degen_head_current);
	void find_tet_degenerates(size_t tet_i);

	face_internal get_face(size_t face)
	{
		face_internal out;
		for (unsigned int i = 0; i < 3; i++)
		{
			const auto& vert = vertices[faces[face].verts[i]];
			out.loc.col(i) = vert.loc;
			out.field.col(i) = vert.field;
		}
		return out;
	}

	tet_internal get_tet(size_t tet)
	{
		tet_internal out;
		for (unsigned int i = 0; i < 4; i++)
		{
			const auto& vert = vertices[tets[tet].verts[i]];
			out.loc.col(i) = vert.loc;
			out.field.col(i) = vert.field;
		}
		return out;
	}

public:
	inline degenerate_curve_tracer(const std::vector<vertex>& vertices_,
	                               const std::vector<face>& faces_,
	                               const std::vector<tet>& tet_);

	std::vector<degen_curve> trace_curves();
};

// Vector containing all of the known degenerate points.
extern std::vector<degen_point> known_degen_points;
extern std::vector<degen_line_segment> known_degen_lines;

// Find the degenerate points that are on cell faces.
void find_infinite_degenerates(const affine_field<3>& field);

inline degenerate_curve_tracer::degenerate_curve_tracer(
	const std::vector<vertex>& vertices_,
	const std::vector<face>& faces_,
	const std::vector<tet>& tets_) :
	vertices(vertices_),
	faces(faces_),
	tets(tets_)
{
}

inline tensor3 matrix_to_tensor3(const Matrix3ft& mat)
{
	tensor3 ret;
	ret << mat(0, 0), mat(0, 1), mat(0, 2),
	       mat(1, 1), mat(1, 2);
	return ret;
}

inline tensor3 matrix_trace_to_tensor3(const Matrix3ft& mat)
{
	tensor3 ret;
	float_type trace_3 = (mat(0, 0) + mat(1, 1) + mat(2, 2)) / 3.0;
	ret << mat(0, 0) - trace_3, mat(0, 1), mat(0, 2),
	       mat(1, 1) - trace_3, mat(1, 2);
	return ret;
}

inline Matrix3ft tensor3_to_matrix(const tensor3& tensor)
{
	Matrix3ft ret;
	ret << tensor[0], tensor[1], tensor[2],
	       tensor[1], tensor[3], tensor[4],
	       tensor[2], tensor[4], -tensor[0] - tensor[3];
	return ret;
}

// Returns a matrix M such that x^T M x = dot(x x^T, tensor), where the dot
// product is the one given by R^tensor_size.
inline Matrix3ft tensor3_to_quadratic_eq(const tensor3& tensor)
{
	Matrix3ft ret;
	ret(0, 0) = (2.0 / 3.0) * tensor[0] - (1.0 / 3.0) * tensor[3];
	ret(1, 1) = (2.0 / 3.0) * tensor[3] - (1.0 / 3.0) * tensor[0];
	ret(2, 2) = (-1.0 / 3.0) * (tensor[0] + tensor[3]);
	ret(0, 1) = ret(1, 0) = 0.5 * tensor[1];
	ret(0, 2) = ret(2, 0) = 0.5 * tensor[2];
	ret(1, 2) = ret(2, 1) = 0.5 * tensor[4];
	return ret;
}

// Normalize barycentric coordinates. Equivalent to multiplying by
// [1 0 0 0], followed by homogeneous division.
// [0 1 0 0]
// [0 0 1 0]
// [0 0 0 1]
// [1 1 1 1]
template<typename Derived>
inline typename Eigen::MatrixBase<Derived>::PlainObject
barycentric_normalized(const Eigen::MatrixBase<Derived>& x)
{
	return x.array().rowwise() * x.array().colwise().sum().cwiseInverse();
}

// A random distribution for tensor3
class tensor_dist : private std::uniform_real_distribution<float_type>
{
	typedef std::uniform_real_distribution<float_type> base_dist;

public:
	typedef tensor3 result_type;
	typedef base_dist::param_type param_type;

	using base_dist::base_dist;

	using base_dist::a;
	using base_dist::b;
	using base_dist::param;

	using base_dist::reset;

	template<typename URNG>
	result_type operator()(URNG& rng)
	{
		tensor3 tensor;
		for (size_t i = 0; i < tensor_size; i++)
		{
			tensor[i] = base_dist::operator()(rng);
		}

		return tensor;
	}

	template<typename URNG>
	result_type operator()(URNG& rng, const param_type& p)
	{
		tensor3 tensor;
		for (size_t i = 0; i < tensor_size; i++)
		{
			tensor[i] = base_dist::operator()(rng, p);
		}

		return tensor;
	}

	friend bool operator==(const tensor_dist& x, const tensor_dist& y)
	{
		return (const base_dist&) x == (const base_dist&) y;
	}
	friend bool operator!=(const tensor_dist& x, const tensor_dist& y)
	{
		return (const base_dist&) x != (const base_dist&) y;
	}

	friend std::ostream& operator<<(std::ostream& os, const tensor_dist& d)
	{
		return os << (const base_dist&) d;
	}
	friend std::istream& operator>>(std::istream& is, tensor_dist& d)
	{
		return is >> (base_dist&) d;
	}
};

void create_tensor_field(const char* file, affine_field<3>& out);
void create_tensor_field_lintens3d(const char* file, affine_field<3>& field);
void create_tensor_field_tetfield(const char* file,
                                  detail::detail_tetrahedron& tet);

#endif
