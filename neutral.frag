#version 430

layout(std140) uniform lightBuf
{
	vec4 uLightPos;
	vec4 uLightSpecularColor;
	vec4 uEyePos;
	float uKa;
	float uKd;
	float uKs;
	float uShininess;
} Light;

in vec3 normal;
in vec3 light;
in vec3 eye;
in vec4 bary_frag;

out vec4 frag_color;

vec3 getLight(vec3 diffuseColor)
{
	vec3 normal = normalize(normal);
	vec3 light  = normalize(light);
	vec3 eye    = normalize(eye);

	// Light both sides.
	normal = gl_FrontFacing ? normal : -normal;

	vec3 ambient = Light.uKa * diffuseColor;
	vec3 diffuse = Light.uKd * max(dot(normal, light), 0.0) * diffuseColor;

	float s = 0.0;
	if (dot(normal, light) > 0.0)
	{
		vec3 ref = reflect(-eye, normal);
		s = Light.uKs * pow(max(dot(ref, light), 0.0), Light.uShininess);
	}
	vec3 specular = s * Light.uLightSpecularColor.rgb;

	return ambient + diffuse + specular;
}

void main()
{
	//vec3 rgb = vec3(0.5, 0.5, 0.5);
	vec3 rgb = abs(bary_frag.xyz);
	//rgb += 0.5 * /*sign(*/bary_frag.yzw/*)*/;
	rgb = getLight(rgb);

	frag_color = vec4(rgb, 1.0);
}
